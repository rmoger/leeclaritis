<?php 


$options['sites'] = array (
  0 => 'gdp.clone.stage.claritis.co.uk',
  1 => 'gdp.dev.p1.claritis.co.uk',
  2 => 'gdptest.p1.claritis.co.uk',
  3 => 'gdp.stage.claritis.co.uk',
);
$options['profiles'] = array (
  0 => 'minimal',
  1 => 'standard',
);
$options['packages'] = array (
  'base' => 
  array (
    'modules' => 
    array (
      'field_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field_ui/field_ui.module',
        'basename' => 'field_ui.module',
        'name' => 'field_ui',
        'info' => 
        array (
          'name' => 'Field UI',
          'description' => 'User interface for the Field API.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_ui.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'openid' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/openid/openid.module',
        'basename' => 'openid.module',
        'name' => 'openid',
        'info' => 
        array (
          'name' => 'OpenID',
          'description' => 'Allows users to log into your site using OpenID.',
          'version' => '7.23',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'openid.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6000',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'path' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/path/path.module',
        'basename' => 'path.module',
        'name' => 'path',
        'info' => 
        array (
          'name' => 'Path',
          'description' => 'Allows users to rename URLs.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'path.test',
          ),
          'configure' => 'admin/config/search/path',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'user' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/user/user.module',
        'basename' => 'user.module',
        'name' => 'user',
        'info' => 
        array (
          'name' => 'User',
          'description' => 'Manages the user registration and login system.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'user.module',
            1 => 'user.test',
          ),
          'required' => true,
          'configure' => 'admin/config/people',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'user.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7018',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'php' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/php/php.module',
        'basename' => 'php.module',
        'name' => 'php',
        'info' => 
        array (
          'name' => 'PHP filter',
          'description' => 'Allows embedded PHP code/snippets to be evaluated.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'php.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'aggregator' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/aggregator/aggregator.module',
        'basename' => 'aggregator.module',
        'name' => 'aggregator',
        'info' => 
        array (
          'name' => 'Aggregator',
          'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'aggregator.test',
          ),
          'configure' => 'admin/config/services/aggregator/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'aggregator.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/toolbar/toolbar.module',
        'basename' => 'toolbar.module',
        'name' => 'toolbar',
        'info' => 
        array (
          'name' => 'Toolbar',
          'description' => 'Provides a toolbar that shows the top-level administration menu items and links from other modules.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.23',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'comment' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/comment/comment.module',
        'basename' => 'comment.module',
        'name' => 'comment',
        'info' => 
        array (
          'name' => 'Comment',
          'description' => 'Allows users to comment on and discuss published content.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'text',
          ),
          'files' => 
          array (
            0 => 'comment.module',
            1 => 'comment.test',
          ),
          'configure' => 'admin/content/comment',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'comment.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7009',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'file' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/file/file.module',
        'basename' => 'file.module',
        'name' => 'file',
        'info' => 
        array (
          'name' => 'File',
          'description' => 'Defines a file field type.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'tests/file.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'syslog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/syslog/syslog.module',
        'basename' => 'syslog.module',
        'name' => 'syslog',
        'info' => 
        array (
          'name' => 'Syslog',
          'description' => 'Logs and records system events to syslog.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'syslog.test',
          ),
          'configure' => 'admin/config/development/logging',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'field_sql_storage' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field/modules/field_sql_storage/field_sql_storage.module',
        'basename' => 'field_sql_storage.module',
        'name' => 'field_sql_storage',
        'info' => 
        array (
          'name' => 'Field SQL storage',
          'description' => 'Stores field data in an SQL database.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_sql_storage.test',
          ),
          'required' => true,
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'list' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field/modules/list/list.module',
        'basename' => 'list.module',
        'name' => 'list',
        'info' => 
        array (
          'name' => 'List',
          'description' => 'Defines list field types. Use with Options to create selection lists.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'options',
          ),
          'files' => 
          array (
            0 => 'tests/list.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'text' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field/modules/text/text.module',
        'basename' => 'text.module',
        'name' => 'text',
        'info' => 
        array (
          'name' => 'Text',
          'description' => 'Defines simple text field types.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'text.test',
          ),
          'required' => true,
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'options' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field/modules/options/options.module',
        'basename' => 'options.module',
        'name' => 'options',
        'info' => 
        array (
          'name' => 'Options',
          'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'options.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'number' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field/modules/number/number.module',
        'basename' => 'number.module',
        'name' => 'number',
        'info' => 
        array (
          'name' => 'Number',
          'description' => 'Defines numeric field types.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'number.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'field' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/field/field.module',
        'basename' => 'field.module',
        'name' => 'field',
        'info' => 
        array (
          'name' => 'Field',
          'description' => 'Field API to add fields to entities like nodes and users.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field.module',
            1 => 'field.attach.inc',
            2 => 'field.info.class.inc',
            3 => 'tests/field.test',
          ),
          'dependencies' => 
          array (
            0 => 'field_sql_storage',
          ),
          'required' => true,
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'theme/field.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'dashboard' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/dashboard/dashboard.module',
        'basename' => 'dashboard.module',
        'name' => 'dashboard',
        'info' => 
        array (
          'name' => 'Dashboard',
          'description' => 'Provides a dashboard page in the administrative interface for organizing administrative tasks and tracking information within your site.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.23',
          'files' => 
          array (
            0 => 'dashboard.test',
          ),
          'dependencies' => 
          array (
            0 => 'block',
          ),
          'configure' => 'admin/dashboard/customize',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/taxonomy/taxonomy.module',
        'basename' => 'taxonomy.module',
        'name' => 'taxonomy',
        'info' => 
        array (
          'name' => 'Taxonomy',
          'description' => 'Enables the categorization of content.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'options',
          ),
          'files' => 
          array (
            0 => 'taxonomy.module',
            1 => 'taxonomy.test',
          ),
          'configure' => 'admin/structure/taxonomy',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7010',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'shortcut' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/shortcut/shortcut.module',
        'basename' => 'shortcut.module',
        'name' => 'shortcut',
        'info' => 
        array (
          'name' => 'Shortcut',
          'description' => 'Allows users to manage customizable lists of shortcut links.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'shortcut.test',
          ),
          'configure' => 'admin/config/user-interface/shortcut',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'filter' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/filter/filter.module',
        'basename' => 'filter.module',
        'name' => 'filter',
        'info' => 
        array (
          'name' => 'Filter',
          'description' => 'Filters content in preparation for display.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'filter.test',
          ),
          'required' => true,
          'configure' => 'admin/config/content/formats',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7010',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'system' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/system/system.module',
        'basename' => 'system.module',
        'name' => 'system',
        'info' => 
        array (
          'name' => 'System',
          'description' => 'Handles general site configuration for administrators.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'system.archiver.inc',
            1 => 'system.mail.inc',
            2 => 'system.queue.inc',
            3 => 'system.tar.inc',
            4 => 'system.updater.inc',
            5 => 'system.test',
          ),
          'required' => true,
          'configure' => 'admin/config/system',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7078',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'translation' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/translation/translation.module',
        'basename' => 'translation.module',
        'name' => 'translation',
        'info' => 
        array (
          'name' => 'Content translation',
          'description' => 'Allows content to be translated into different languages.',
          'dependencies' => 
          array (
            0 => 'locale',
          ),
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'translation.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'trigger' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/trigger/trigger.module',
        'basename' => 'trigger.module',
        'name' => 'trigger',
        'info' => 
        array (
          'name' => 'Trigger',
          'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'trigger.test',
          ),
          'configure' => 'admin/structure/trigger',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'dblog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/dblog/dblog.module',
        'basename' => 'dblog.module',
        'name' => 'dblog',
        'info' => 
        array (
          'name' => 'Database logging',
          'description' => 'Logs and records system events to the database.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'dblog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'tracker' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/tracker/tracker.module',
        'basename' => 'tracker.module',
        'name' => 'tracker',
        'info' => 
        array (
          'name' => 'Tracker',
          'description' => 'Enables tracking of recent content for users.',
          'dependencies' => 
          array (
            0 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tracker.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'block' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/block/block.module',
        'basename' => 'block.module',
        'name' => 'block',
        'info' => 
        array (
          'name' => 'Block',
          'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'block.test',
          ),
          'configure' => 'admin/structure/block',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7008',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'statistics' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/statistics/statistics.module',
        'basename' => 'statistics.module',
        'name' => 'statistics',
        'info' => 
        array (
          'name' => 'Statistics',
          'description' => 'Logs access statistics for your site.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'statistics.test',
          ),
          'configure' => 'admin/config/system/statistics',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'update' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/update/update.module',
        'basename' => 'update.module',
        'name' => 'update',
        'info' => 
        array (
          'name' => 'Update manager',
          'description' => 'Checks for available updates, and can securely install or update modules and themes via a web interface.',
          'version' => '7.23',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'update.test',
          ),
          'configure' => 'admin/reports/updates/settings',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'node' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/node/node.module',
        'basename' => 'node.module',
        'name' => 'node',
        'info' => 
        array (
          'name' => 'Node',
          'description' => 'Allows content to be submitted to the site and displayed on pages.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'node.module',
            1 => 'node.test',
          ),
          'required' => true,
          'configure' => 'admin/structure/types',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'node.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7013',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'rdf' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/rdf/rdf.module',
        'basename' => 'rdf.module',
        'name' => 'rdf',
        'info' => 
        array (
          'name' => 'RDF',
          'description' => 'Enriches your content with metadata to let other applications (e.g. search engines, aggregators) better understand its relationships and attributes.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rdf.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'locale' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/locale/locale.module',
        'basename' => 'locale.module',
        'name' => 'locale',
        'info' => 
        array (
          'name' => 'Locale',
          'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'locale.test',
          ),
          'configure' => 'admin/config/regional/language',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'forum' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/forum/forum.module',
        'basename' => 'forum.module',
        'name' => 'forum',
        'info' => 
        array (
          'name' => 'Forum',
          'description' => 'Provides discussion forums.',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'forum.test',
          ),
          'configure' => 'admin/structure/forum',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'forum.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7012',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'overlay' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/overlay/overlay.module',
        'basename' => 'overlay.module',
        'name' => 'overlay',
        'info' => 
        array (
          'name' => 'Overlay',
          'description' => 'Displays the Drupal administration interface in an overlay.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'book' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/book/book.module',
        'basename' => 'book.module',
        'name' => 'book',
        'info' => 
        array (
          'name' => 'Book',
          'description' => 'Allows users to create and organize related content in an outline.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'book.test',
          ),
          'configure' => 'admin/content/book/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'book.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'help' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/help/help.module',
        'basename' => 'help.module',
        'name' => 'help',
        'info' => 
        array (
          'name' => 'Help',
          'description' => 'Manages the display of online help.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'help.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'color' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/color/color.module',
        'basename' => 'color.module',
        'name' => 'color',
        'info' => 
        array (
          'name' => 'Color',
          'description' => 'Allows administrators to change the color scheme of compatible themes.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'color.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'search' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/search/search.module',
        'basename' => 'search.module',
        'name' => 'search',
        'info' => 
        array (
          'name' => 'Search',
          'description' => 'Enables site-wide keyword searching.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'search.extender.inc',
            1 => 'search.test',
          ),
          'configure' => 'admin/config/search/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'search.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'image' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/image/image.module',
        'basename' => 'image.module',
        'name' => 'image',
        'info' => 
        array (
          'name' => 'Image',
          'description' => 'Provides image manipulation tools.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'files' => 
          array (
            0 => 'image.test',
          ),
          'configure' => 'admin/config/media/image-styles',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/simpletest/simpletest.module',
        'basename' => 'simpletest.module',
        'name' => 'simpletest',
        'info' => 
        array (
          'name' => 'Testing',
          'description' => 'Provides a framework for unit and functional testing.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'simpletest.test',
            1 => 'drupal_web_test_case.php',
            2 => 'tests/actions.test',
            3 => 'tests/ajax.test',
            4 => 'tests/batch.test',
            5 => 'tests/bootstrap.test',
            6 => 'tests/cache.test',
            7 => 'tests/common.test',
            8 => 'tests/database_test.test',
            9 => 'tests/entity_crud_hook_test.test',
            10 => 'tests/entity_query.test',
            11 => 'tests/error.test',
            12 => 'tests/file.test',
            13 => 'tests/filetransfer.test',
            14 => 'tests/form.test',
            15 => 'tests/graph.test',
            16 => 'tests/image.test',
            17 => 'tests/lock.test',
            18 => 'tests/mail.test',
            19 => 'tests/menu.test',
            20 => 'tests/module.test',
            21 => 'tests/pager.test',
            22 => 'tests/password.test',
            23 => 'tests/path.test',
            24 => 'tests/registry.test',
            25 => 'tests/schema.test',
            26 => 'tests/session.test',
            27 => 'tests/tablesort.test',
            28 => 'tests/theme.test',
            29 => 'tests/unicode.test',
            30 => 'tests/update.test',
            31 => 'tests/xmlrpc.test',
            32 => 'tests/upgrade/upgrade.test',
            33 => 'tests/upgrade/upgrade.comment.test',
            34 => 'tests/upgrade/upgrade.filter.test',
            35 => 'tests/upgrade/upgrade.forum.test',
            36 => 'tests/upgrade/upgrade.locale.test',
            37 => 'tests/upgrade/upgrade.menu.test',
            38 => 'tests/upgrade/upgrade.node.test',
            39 => 'tests/upgrade/upgrade.taxonomy.test',
            40 => 'tests/upgrade/upgrade.trigger.test',
            41 => 'tests/upgrade/upgrade.translatable.test',
            42 => 'tests/upgrade/upgrade.upload.test',
            43 => 'tests/upgrade/upgrade.user.test',
            44 => 'tests/upgrade/update.aggregator.test',
            45 => 'tests/upgrade/update.trigger.test',
            46 => 'tests/upgrade/update.field.test',
            47 => 'tests/upgrade/update.user.test',
          ),
          'configure' => 'admin/config/development/testing/settings',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'blog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/blog/blog.module',
        'basename' => 'blog.module',
        'name' => 'blog',
        'info' => 
        array (
          'name' => 'Blog',
          'description' => 'Enables multi-user blogs.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'blog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'menu' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/menu/menu.module',
        'basename' => 'menu.module',
        'name' => 'menu',
        'info' => 
        array (
          'name' => 'Menu',
          'description' => 'Allows administrators to customize the site navigation menu.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'menu.test',
          ),
          'configure' => 'admin/structure/menu',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'poll' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/poll/poll.module',
        'basename' => 'poll.module',
        'name' => 'poll',
        'info' => 
        array (
          'name' => 'Poll',
          'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'poll.test',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'poll.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'contact' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/contact/contact.module',
        'basename' => 'contact.module',
        'name' => 'contact',
        'info' => 
        array (
          'name' => 'Contact',
          'description' => 'Enables the use of both personal and site-wide contact forms.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contact.test',
          ),
          'configure' => 'admin/structure/contact',
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'contextual' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/modules/contextual/contextual.module',
        'basename' => 'contextual.module',
        'name' => 'contextual',
        'info' => 
        array (
          'name' => 'Contextual links',
          'description' => 'Provides contextual links to perform actions related to elements on a page.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contextual.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.23',
      ),
    ),
    'themes' => 
    array (
      'bartik' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/themes/bartik/bartik.info',
        'basename' => 'bartik.info',
        'name' => 'Bartik',
        'info' => 
        array (
          'name' => 'Bartik',
          'description' => 'A flexible, recolorable theme with many regions.',
          'package' => 'Core',
          'version' => '7.22',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/layout.css',
              1 => 'css/style.css',
              2 => 'css/colors.css',
            ),
            'print' => 
            array (
              0 => 'css/print.css',
            ),
          ),
          'regions' => 
          array (
            'header' => 'Header',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'highlighted' => 'Highlighted',
            'featured' => 'Featured',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'sidebar_second' => 'Sidebar second',
            'triptych_first' => 'Triptych first',
            'triptych_middle' => 'Triptych middle',
            'triptych_last' => 'Triptych last',
            'footer_firstcolumn' => 'Footer first column',
            'footer_secondcolumn' => 'Footer second column',
            'footer_thirdcolumn' => 'Footer third column',
            'footer_fourthcolumn' => 'Footer fourth column',
            'footer' => 'Footer',
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '0',
          ),
          'project' => 'drupal',
          'datestamp' => '1365027012',
        ),
        'project' => 'drupal',
        'version' => '7.22',
      ),
      'seven' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/themes/seven/seven.info',
        'basename' => 'seven.info',
        'name' => 'Seven',
        'info' => 
        array (
          'name' => 'Seven',
          'description' => 'A simple one-column, tableless, fluid width administration theme.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'reset.css',
              1 => 'style.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'sidebar_first' => 'First sidebar',
          ),
          'regions_hidden' => 
          array (
            0 => 'sidebar_first',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
        ),
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'stark' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/themes/stark/stark.info',
        'basename' => 'stark.info',
        'name' => 'Stark',
        'info' => 
        array (
          'name' => 'Stark',
          'description' => 'This theme demonstrates Drupal\'s default HTML markup and CSS styles. To learn how to build your own theme and override Drupal\'s default code, see the <a href="http://drupal.org/theme-guide">Theming Guide</a>.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'layout.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
        ),
        'project' => 'drupal',
        'version' => '7.23',
      ),
      'garland' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/themes/garland/garland.info',
        'basename' => 'garland.info',
        'name' => 'Garland',
        'info' => 
        array (
          'name' => 'Garland',
          'description' => 'A multi-column theme which can be configured to modify colors and switch between fixed and fluid width layouts.',
          'package' => 'Core',
          'version' => '7.23',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'style.css',
            ),
            'print' => 
            array (
              0 => 'print.css',
            ),
          ),
          'settings' => 
          array (
            'garland_width' => 'fluid',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
        ),
        'project' => 'drupal',
        'version' => '7.23',
      ),
    ),
    'platforms' => 
    array (
      'drupal' => 
      array (
        'short_name' => 'drupal',
        'version' => '7.23',
        'description' => 'This platform is running Drupal 7.23',
      ),
    ),
    'profiles' => 
    array (
      'minimal' => 
      array (
        'name' => 'minimal',
        'filename' => '/var/aegir/platforms/gdp.stage/profiles/minimal/minimal.profile',
        'project' => 'drupal',
        'info' => 
        array (
          'name' => 'Minimal',
          'description' => 'Start with only a few modules enabled.',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'dblog',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
        ),
        'version' => '7.23',
      ),
      'standard' => 
      array (
        'name' => 'standard',
        'filename' => '/var/aegir/platforms/gdp.stage/profiles/standard/standard.profile',
        'project' => 'drupal',
        'info' => 
        array (
          'name' => 'Standard',
          'description' => 'Install with commonly used features pre-configured.',
          'version' => '7.23',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'color',
            2 => 'comment',
            3 => 'contextual',
            4 => 'dashboard',
            5 => 'help',
            6 => 'image',
            7 => 'list',
            8 => 'menu',
            9 => 'number',
            10 => 'options',
            11 => 'path',
            12 => 'taxonomy',
            13 => 'dblog',
            14 => 'search',
            15 => 'shortcut',
            16 => 'toolbar',
            17 => 'overlay',
            18 => 'field_ui',
            19 => 'file',
            20 => 'rdf',
          ),
          'project' => 'drupal',
          'datestamp' => '1375928238',
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
          'old_short_name' => 'default',
        ),
        'version' => '7.23',
      ),
    ),
  ),
  'sites-all' => 
  array (
    'modules' => 
    array (
      'context_layouts' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/context/context_layouts/context_layouts.module',
        'basename' => 'context_layouts.module',
        'name' => 'context_layouts',
        'info' => 
        array (
          'name' => 'Context layouts',
          'description' => 'Allow theme layer to provide multiple region layouts and integrate with context.',
          'dependencies' => 
          array (
            0 => 'context',
          ),
          'package' => 'Context',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/context_layouts_reaction_block.inc',
          ),
          'version' => '7.x-3.0-beta6',
          'project' => 'context',
          'datestamp' => '1355879811',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'context',
        'version' => '7.x-3.0-beta6',
      ),
      'context_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/context/context_ui/context_ui.module',
        'basename' => 'context_ui.module',
        'name' => 'context_ui',
        'info' => 
        array (
          'name' => 'Context UI',
          'description' => 'Provides a simple UI for settings up a site structure using Context.',
          'dependencies' => 
          array (
            0 => 'context',
          ),
          'package' => 'Context',
          'core' => '7.x',
          'configure' => 'admin/structure/context',
          'files' => 
          array (
            0 => 'context.module',
            1 => 'tests/context_ui.test',
          ),
          'version' => '7.x-3.0-beta6',
          'project' => 'context',
          'datestamp' => '1355879811',
          'php' => '5.2.4',
        ),
        'schema_version' => '6004',
        'project' => 'context',
        'version' => '7.x-3.0-beta6',
      ),
      'context' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/context/context.module',
        'basename' => 'context.module',
        'name' => 'context',
        'info' => 
        array (
          'name' => 'Context',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'description' => 'Provide modules with a cache that lasts for a single page request.',
          'package' => 'Context',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'context.module',
            1 => 'tests/context.test',
            2 => 'tests/context.conditions.test',
            3 => 'tests/context.reactions.test',
          ),
          'version' => '7.x-3.0-beta6',
          'project' => 'context',
          'datestamp' => '1355879811',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'context',
        'version' => '7.x-3.0-beta6',
      ),
      'treaties2' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/features/treaties2/treaties2.module',
        'basename' => 'treaties2.module',
        'name' => 'treaties2',
        'info' => 
        array (
          'name' => 'treaties',
          'core' => '7.x',
          'package' => 'Features',
          'dependencies' => 
          array (
            0 => 'node',
            1 => 'text',
          ),
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'strongarm:strongarm:1',
            ),
            'features_api' => 
            array (
              0 => 'api:1',
            ),
            'field_base' => 
            array (
              0 => 'field_adoption_year',
              1 => 'field_full_title',
              2 => 'field_instrument_type',
              3 => 'field_treaty_region',
            ),
            'field_instance' => 
            array (
              0 => 'node-treaty-body',
              1 => 'node-treaty-field_full_title',
              2 => 'node-treaty-field_instrument_type',
              3 => 'node-treaty-field_treaty_region',
            ),
            'node' => 
            array (
              0 => 'treaty',
            ),
          ),
          'features_exclude' => 
          array (
            'dependencies' => 
            array (
              'ctools' => 'ctools',
              'date' => 'date',
              'features' => 'features',
              'list' => 'list',
              'options' => 'options',
              'strongarm' => 'strongarm',
            ),
            'field_base' => 
            array (
              'body' => 'body',
            ),
            'field_instance' => 
            array (
              'node-treaty-field_adoption_year' => 'node-treaty-field_adoption_year',
            ),
            'field' => 
            array (
              'node-treaty-body' => 'node-treaty-body',
              'node-treaty-field_treaty_region' => 'node-treaty-field_treaty_region',
              'node-treaty-field_full_title' => 'node-treaty-field_full_title',
              'node-treaty-field_instrument_type' => 'node-treaty-field_instrument_type',
              'node-treaty-field_adoption_year' => 'node-treaty-field_adoption_year',
            ),
            'variable' => 
            array (
              'comment_anonymous_treaty' => 'comment_anonymous_treaty',
              'comment_default_mode_treaty' => 'comment_default_mode_treaty',
              'comment_default_per_page_treaty' => 'comment_default_per_page_treaty',
              'comment_form_location_treaty' => 'comment_form_location_treaty',
              'comment_preview_treaty' => 'comment_preview_treaty',
              'comment_subject_field_treaty' => 'comment_subject_field_treaty',
              'comment_treaty' => 'comment_treaty',
              'field_bundle_settings_node__treaty' => 'field_bundle_settings_node__treaty',
              'menu_options_treaty' => 'menu_options_treaty',
              'menu_parent_treaty' => 'menu_parent_treaty',
              'node_options_treaty' => 'node_options_treaty',
              'node_preview_treaty' => 'node_preview_treaty',
              'node_submitted_treaty' => 'node_submitted_treaty',
            ),
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'date_repeat_field' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_repeat_field/date_repeat_field.module',
        'basename' => 'date_repeat_field.module',
        'name' => 'date_repeat_field',
        'info' => 
        array (
          'name' => 'Date Repeat Field',
          'description' => 'Creates the option of Repeating date fields and manages Date fields that use the Date Repeat API.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
            2 => 'date_repeat',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date_repeat_field.css',
            ),
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_views' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_views/date_views.module',
        'basename' => 'date_views.module',
        'name' => 'date_views',
        'info' => 
        array (
          'name' => 'Date Views',
          'description' => 'Views integration for date fields and date functionality.',
          'package' => 'Date/Time',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'views',
          ),
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'includes/date_views_argument_handler.inc',
            1 => 'includes/date_views_argument_handler_simple.inc',
            2 => 'includes/date_views_filter_handler.inc',
            3 => 'includes/date_views_filter_handler_simple.inc',
            4 => 'includes/date_views.views_default.inc',
            5 => 'includes/date_views.views.inc',
            6 => 'includes/date_views_plugin_pager.inc',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_api' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_api/date_api.module',
        'basename' => 'date_api.module',
        'name' => 'date_api',
        'info' => 
        array (
          'name' => 'Date API',
          'description' => 'A Date API that can be used by other modules.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date.css',
            ),
          ),
          'files' => 
          array (
            0 => 'date_api.module',
            1 => 'date_api_sql.inc',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'dependencies' => 
          array (
          ),
        ),
        'schema_version' => '7001',
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_migrate_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_migrate/date_migrate_example/date_migrate_example.module',
        'basename' => 'date_migrate_example.module',
        'name' => 'date_migrate_example',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'date_repeat',
            2 => 'date_repeat_field',
            3 => 'date_migrate',
            4 => 'features',
            5 => 'migrate',
          ),
          'description' => 'Examples of migrating with the Date module',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-date_migrate_example-body',
              1 => 'node-date_migrate_example-field_date',
              2 => 'node-date_migrate_example-field_date_range',
              3 => 'node-date_migrate_example-field_date_repeat',
              4 => 'node-date_migrate_example-field_datestamp',
              5 => 'node-date_migrate_example-field_datestamp_range',
              6 => 'node-date_migrate_example-field_datetime',
              7 => 'node-date_migrate_example-field_datetime_range',
            ),
            'node' => 
            array (
              0 => 'date_migrate_example',
            ),
          ),
          'files' => 
          array (
            0 => 'date_migrate_example.migrate.inc',
          ),
          'name' => 'Date Migration Example',
          'package' => 'Features',
          'project' => 'date',
          'version' => '7.x-2.6',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_migrate/date_migrate.module',
        'basename' => 'date_migrate.module',
        'name' => 'date_migrate',
        'info' => 
        array (
          'name' => 'Date Migration',
          'description' => 'Provides support for importing into date fields with the Migrate module.',
          'core' => '7.x',
          'package' => 'Date/Time',
          'dependencies' => 
          array (
            0 => 'migrate',
            1 => 'date',
          ),
          'files' => 
          array (
            0 => 'date.migrate.inc',
            1 => 'date_migrate.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_popup' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_popup/date_popup.module',
        'basename' => 'date_popup.module',
        'name' => 'date_popup',
        'info' => 
        array (
          'name' => 'Date Popup',
          'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/date_popup',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'themes/datepicker.1.7.css',
            ),
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_context' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_context/date_context.module',
        'basename' => 'date_context.module',
        'name' => 'date_context',
        'info' => 
        array (
          'name' => 'Date Context',
          'description' => 'Adds an option to the Context module to set a context condition based on the value of a date field.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'context',
          ),
          'files' => 
          array (
            0 => 'date_context.module',
            1 => 'plugins/date_context_date_condition.inc',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_repeat' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_repeat/date_repeat.module',
        'basename' => 'date_repeat.module',
        'name' => 'date_repeat',
        'info' => 
        array (
          'name' => 'Date Repeat API',
          'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'tests/date_repeat.test',
            1 => 'tests/date_repeat_form.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_all_day' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_all_day/date_all_day.module',
        'basename' => 'date_all_day.module',
        'name' => 'date_all_day',
        'info' => 
        array (
          'name' => 'Date All Day',
          'description' => 'Adds \'All Day\' functionality to date fields, including an \'All Day\' theme and \'All Day\' checkboxes for the Date select and Date popup widgets.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date_tools/date_tools.module',
        'basename' => 'date_tools.module',
        'name' => 'date_tools',
        'info' => 
        array (
          'name' => 'Date Tools',
          'description' => 'Tools to import and auto-create dates and calendars.',
          'dependencies' => 
          array (
            0 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/tools',
          'files' => 
          array (
            0 => 'tests/date_tools.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/date/date.module',
        'basename' => 'date.module',
        'name' => 'date',
        'info' => 
        array (
          'name' => 'Date',
          'description' => 'Makes date/time fields available.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'tests/date_api.test',
            1 => 'tests/date.test',
            2 => 'tests/date_field.test',
            3 => 'tests/date_validation.test',
            4 => 'tests/date_timezone.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
        ),
        'schema_version' => '7004',
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'views_content' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/views_content/views_content.module',
        'basename' => 'views_content.module',
        'name' => 'views_content',
        'info' => 
        array (
          'name' => 'Views content panes',
          'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'views',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
            1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
            2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'stylizer' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/stylizer/stylizer.module',
        'basename' => 'stylizer.module',
        'name' => 'stylizer',
        'info' => 
        array (
          'name' => 'Stylizer',
          'description' => 'Create custom styles for applications such as Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'color',
          ),
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'ctools_custom_content' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
        'basename' => 'ctools_custom_content.module',
        'name' => 'ctools_custom_content',
        'info' => 
        array (
          'name' => 'Custom content panes',
          'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'ctools_access_ruleset' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
        'basename' => 'ctools_access_ruleset.module',
        'name' => 'ctools_access_ruleset',
        'info' => 
        array (
          'name' => 'Custom rulesets',
          'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'ctools_ajax_sample' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
        'basename' => 'ctools_ajax_sample.module',
        'name' => 'ctools_ajax_sample',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) AJAX Example',
          'description' => 'Shows how to use the power of Chaos AJAX.',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'core' => '7.x',
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'ctools_plugin_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
        'basename' => 'ctools_plugin_example.module',
        'name' => 'ctools_plugin_example',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) Plugin Example',
          'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'panels',
            2 => 'page_manager',
            3 => 'advanced_help',
          ),
          'core' => '7.x',
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'bulk_export' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/bulk_export/bulk_export.module',
        'basename' => 'bulk_export.module',
        'name' => 'bulk_export',
        'info' => 
        array (
          'name' => 'Bulk Export',
          'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'page_manager' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/page_manager/page_manager.module',
        'basename' => 'page_manager.module',
        'name' => 'page_manager',
        'info' => 
        array (
          'name' => 'Page manager',
          'description' => 'Provides a UI and API to manage pages within the site.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'ctools' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ctools/ctools.module',
        'basename' => 'ctools.module',
        'name' => 'ctools',
        'info' => 
        array (
          'name' => 'Chaos tools',
          'description' => 'A library of helpful tools by Merlin of Chaos.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'files' => 
          array (
            0 => 'includes/context.inc',
            1 => 'includes/math-expr.inc',
            2 => 'includes/stylizer.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'ctools',
          'datestamp' => '1365013512',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6008',
        'project' => 'ctools',
        'version' => '7.x-1.3',
      ),
      'imagefield_tokens' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/imagefield_tokens/imagefield_tokens.module',
        'basename' => 'imagefield_tokens.module',
        'name' => 'imagefield_tokens',
        'info' => 
        array (
          'name' => 'ImageField Tokens',
          'description' => 'Adds additional settings to the Image field alt/title attributes.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.x-dev',
          'project' => 'imagefield_tokens',
          'datestamp' => '1380582815',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'imagefield_tokens',
        'version' => '7.x-1.x-dev',
      ),
      'bundle_copy' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/bundle_copy/bundle_copy.module',
        'basename' => 'bundle_copy.module',
        'name' => 'bundle_copy',
        'info' => 
        array (
          'name' => 'Bundle copy',
          'description' => 'Import and exports bundles through the UI.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'bundle_copy.module',
          ),
          'version' => '7.x-1.1',
          'project' => 'bundle_copy',
          'datestamp' => '1332926440',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'bundle_copy',
        'version' => '7.x-1.1',
      ),
      'admin' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/admin/admin.module',
        'basename' => 'admin.module',
        'name' => 'admin',
        'info' => 
        array (
          'name' => 'Admin',
          'description' => 'UI helpers for Drupal admins and managers.',
          'package' => 'Administration',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'admin.admin.inc',
            1 => 'admin.install',
            2 => 'admin.module',
            3 => 'includes/admin.devel.inc',
            4 => 'includes/admin.theme.inc',
            5 => 'theme/admin-panes.tpl.php',
            6 => 'theme/admin-toolbar.tpl.php',
            7 => 'theme/theme.inc',
          ),
          'version' => '7.x-2.0-beta3',
          'project' => 'admin',
          'datestamp' => '1292541646',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'admin',
        'version' => '7.x-2.0-beta3',
      ),
      'field_group' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_group/field_group.module',
        'basename' => 'field_group.module',
        'name' => 'field_group',
        'info' => 
        array (
          'name' => 'Fieldgroup',
          'description' => 'Fieldgroup',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'ctools',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_group.install',
            1 => 'field_group.module',
            2 => 'field_group.field_ui.inc',
            3 => 'field_group.form.inc',
            4 => 'field_group.features.inc',
            5 => 'field_group.test',
          ),
          'version' => '7.x-1.1',
          'project' => 'field_group',
          'datestamp' => '1319051133',
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'field_group',
        'version' => '7.x-1.1',
      ),
      'edit' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/edit/edit.module',
        'basename' => 'edit.module',
        'name' => 'edit',
        'info' => 
        array (
          'name' => 'Edit',
          'description' => 'In-place content editing.',
          'package' => 'Spark',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'field (>=7.22)',
          ),
          'version' => '7.x-1.0-alpha11+0-dev',
          'project' => 'edit',
          'datestamp' => '1366419210',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'edit',
        'version' => '7.x-1.0-alpha11+0-dev',
      ),
      'rel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/rel/rel.module',
        'basename' => 'rel.module',
        'name' => 'rel',
        'info' => 
        array (
          'name' => 'Renderable elements',
          'description' => 'Register any forms to enable you to manage the display through an UI',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'rel.module',
            1 => 'rel.test',
            2 => 'rel.admin.inc',
          ),
          'version' => '7.x-1.0-alpha2',
          'project' => 'rel',
          'datestamp' => '1358839857',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'rel',
        'version' => '7.x-1.0-alpha2',
      ),
      'memcache_admin' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/memcache/memcache_admin/memcache_admin.module',
        'basename' => 'memcache_admin.module',
        'name' => 'memcache_admin',
        'info' => 
        array (
          'name' => 'Memcache Admin',
          'description' => 'Adds a User Interface to monitor the Memcache for this site.',
          'core' => '7.x',
          'package' => 'Performance and scalability',
          'version' => '7.x-1.0',
          'project' => 'memcache',
          'datestamp' => '1326973845',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'memcache',
        'version' => '7.x-1.0',
      ),
      'memcache' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/memcache/memcache.module',
        'basename' => 'memcache.module',
        'name' => 'memcache',
        'info' => 
        array (
          'name' => 'Memcache',
          'description' => 'High performance integration with memcache.',
          'package' => 'Performance and scalability',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/memcache.test',
            1 => 'tests/memcache-session.test',
            2 => 'tests/memcache-lock.test',
          ),
          'version' => '7.x-1.0',
          'project' => 'memcache',
          'datestamp' => '1326973845',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'memcache',
        'version' => '7.x-1.0',
      ),
      'xhprof_mongodb' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/XHProf/modules/xhprof_mongodb/xhprof_mongodb.module',
        'basename' => 'xhprof_mongodb.module',
        'name' => 'xhprof_mongodb',
        'info' => 
        array (
          'name' => 'XHProf MongoDB',
          'description' => 'UI for xhprof runs.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'mongodb',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'MongodbXHProfRuns.inc',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'XHProf',
          'datestamp' => '1322014244',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'XHProf',
        'version' => '7.x-1.0-beta2',
      ),
      'xhprof' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/XHProf/xhprof.module',
        'basename' => 'xhprof.module',
        'name' => 'xhprof',
        'info' => 
        array (
          'name' => 'xhprof',
          'description' => 'XHProf integration.',
          'package' => 'Development',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'XHProfRunsInterface.inc',
            1 => 'XHProfRunsFile.inc',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'XHProf',
          'datestamp' => '1322014244',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'XHProf',
        'version' => '7.x-1.0-beta2',
      ),
      'feeds_xls' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds_xls/feeds_xls.module',
        'basename' => 'feeds_xls.module',
        'name' => 'feeds_xls',
        'info' => 
        array (
          'name' => 'Feeds XLS',
          'description' => 'Excel file parser for importing excel data via feeds.module',
          'package' => 'Feeds',
          'dependencies' => 
          array (
            0 => 'feeds',
          ),
          'files' => 
          array (
            0 => 'FeedsExcelParser.inc',
          ),
          'core' => '7.x',
          'version' => '7.x-0.0',
          'project' => 'feeds_xls',
          'datestamp' => '1317161804',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'feeds_xls',
        'version' => '7.x-0.0',
      ),
      'jquery_update' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/jquery_update/jquery_update.module',
        'basename' => 'jquery_update.module',
        'name' => 'jquery_update',
        'info' => 
        array (
          'name' => 'jQuery Update',
          'description' => 'Update jQuery and jQuery UI to a more recent version.',
          'package' => 'User interface',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'jquery_update.module',
            1 => 'jquery_update.install',
          ),
          'configure' => 'admin/config/development/jquery_update',
          'version' => '7.x-2.3',
          'project' => 'jquery_update',
          'datestamp' => '1360375905',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'jquery_update',
        'version' => '7.x-2.3',
      ),
      'uuid_services' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/uuid/uuid_services/uuid_services.module',
        'basename' => 'uuid_services.module',
        'name' => 'uuid_services',
        'info' => 
        array (
          'name' => 'UUID Services',
          'description' => 'Provides integration with the Services module, like exposing a UUID entity resource.',
          'core' => '7.x',
          'package' => 'Services - resources',
          'dependencies' => 
          array (
            0 => 'services',
            1 => 'uuid',
            2 => 'entity',
          ),
          'version' => '7.x-1.0-alpha5',
          'project' => 'uuid',
          'datestamp' => '1373620283',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'uuid',
        'version' => '7.x-1.0-alpha5',
      ),
      'uuid_services_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/uuid/uuid_services_example/uuid_services_example.module',
        'basename' => 'uuid_services_example.module',
        'name' => 'uuid_services_example',
        'info' => 
        array (
          'name' => 'UUID Services Example',
          'description' => 'Example feature of a UUID service. Works well with the Deploy Example feature as a client.',
          'core' => '7.x',
          'package' => 'Features',
          'php' => '5.2.4',
          'dependencies' => 
          array (
            0 => 'rest_server',
            1 => 'services',
            2 => 'uuid',
            3 => 'uuid_services',
          ),
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'services:services:3',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'services_endpoint' => 
            array (
              0 => 'uuid_services_example',
            ),
          ),
          'version' => '7.x-1.0-alpha5',
          'project' => 'uuid',
          'datestamp' => '1373620283',
        ),
        'schema_version' => 0,
        'project' => 'uuid',
        'version' => '7.x-1.0-alpha5',
      ),
      'uuid_default_entities_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/uuid/uuid_default_entities_example/uuid_default_entities_example.module',
        'basename' => 'uuid_default_entities_example.module',
        'name' => 'uuid_default_entities_example',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'features',
            2 => 'uuid',
          ),
          'description' => 'Example feature mainly used for testing.',
          'features' => 
          array (
            'uuid_entities' => 
            array (
              0 => 'deploy_example_plan',
            ),
          ),
          'name' => 'UUID default entities example',
          'package' => 'Features',
          'version' => '7.x-1.0-alpha5',
          'project' => 'uuid',
          'datestamp' => '1373620283',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'uuid',
        'version' => '7.x-1.0-alpha5',
      ),
      'uuid_path' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/uuid/uuid_path/uuid_path.module',
        'basename' => 'uuid_path.module',
        'name' => 'uuid_path',
        'info' => 
        array (
          'name' => 'UUID Path',
          'description' => 'Provides export functionality for url aliases.',
          'core' => '7.x',
          'package' => 'UUID',
          'dependencies' => 
          array (
            0 => 'uuid',
          ),
          'version' => '7.x-1.0-alpha5',
          'project' => 'uuid',
          'datestamp' => '1373620283',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'uuid',
        'version' => '7.x-1.0-alpha5',
      ),
      'uuid' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/uuid/uuid.module',
        'basename' => 'uuid.module',
        'name' => 'uuid',
        'info' => 
        array (
          'name' => 'Universally Unique ID',
          'description' => 'Extends the entity functionality and adds support for universally unique identifiers.',
          'core' => '7.x',
          'package' => 'UUID',
          'configure' => 'admin/config/system/uuid',
          'files' => 
          array (
            0 => 'uuid.test',
          ),
          'version' => '7.x-1.0-alpha5',
          'project' => 'uuid',
          'datestamp' => '1373620283',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'project' => 'uuid',
        'version' => '7.x-1.0-alpha5',
      ),
      'nodeaccess' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/nodeaccess/nodeaccess.module',
        'basename' => 'nodeaccess.module',
        'name' => 'nodeaccess',
        'info' => 
        array (
          'name' => 'Nodeaccess',
          'description' => 'Provides per node access control',
          'core' => '7.x',
          'package' => 'Access control',
          'configure' => 'admin/config/people/nodeaccess',
          'version' => '7.x-1.0',
          'project' => 'nodeaccess',
          'datestamp' => '1367936720',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'nodeaccess',
        'version' => '7.x-1.0',
      ),
      'geophp' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/geophp/geophp.module',
        'basename' => 'geophp.module',
        'name' => 'geophp',
        'info' => 
        array (
          'name' => 'geoPHP',
          'description' => 'Wraps the geoPHP library: advanced geometry operations in PHP',
          'core' => '7.x',
          'version' => '7.x-1.7',
          'project' => 'geophp',
          'datestamp' => '1352084822',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'geophp',
        'version' => '7.x-1.7',
      ),
      'job_scheduler_trigger' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/job_scheduler/modules/job_scheduler_trigger/job_scheduler_trigger.module',
        'basename' => 'job_scheduler_trigger.module',
        'name' => 'job_scheduler_trigger',
        'info' => 
        array (
          'name' => 'Job Scheduler Trigger',
          'description' => 'Creates scheduler triggers that fire up at certain days, times',
          'core' => '7.x',
          'php' => '5.2',
          'dependencies' => 
          array (
            0 => 'job_scheduler',
          ),
          'version' => '7.x-2.0-alpha3',
          'project' => 'job_scheduler',
          'datestamp' => '1336466457',
        ),
        'schema_version' => 0,
        'project' => 'job_scheduler',
        'version' => '7.x-2.0-alpha3',
      ),
      'job_scheduler' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/job_scheduler/job_scheduler.module',
        'basename' => 'job_scheduler.module',
        'name' => 'job_scheduler',
        'info' => 
        array (
          'name' => 'Job Scheduler',
          'description' => 'Scheduler API',
          'files' => 
          array (
            0 => 'job_scheduler.module',
            1 => 'job_scheduler.install',
            2 => 'JobScheduler.inc',
            3 => 'JobSchedulerCronTab.inc',
          ),
          'core' => '7.x',
          'php' => '5.2',
          'version' => '7.x-2.0-alpha3',
          'project' => 'job_scheduler',
          'datestamp' => '1336466457',
          'dependencies' => 
          array (
          ),
        ),
        'schema_version' => '7101',
        'project' => 'job_scheduler',
        'version' => '7.x-2.0-alpha3',
      ),
      'diff' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/diff/diff.module',
        'basename' => 'diff.module',
        'name' => 'diff',
        'info' => 
        array (
          'name' => 'Diff',
          'description' => 'Show differences between content revisions.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'DiffEngine.php',
          ),
          'version' => '7.x-3.2',
          'project' => 'diff',
          'datestamp' => '1352784357',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7305',
        'project' => 'diff',
        'version' => '7.x-3.2',
      ),
      'module_filter' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/module_filter/module_filter.module',
        'basename' => 'module_filter.module',
        'name' => 'module_filter',
        'info' => 
        array (
          'name' => 'Module filter',
          'description' => 'Filter the modules list.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'module_filter.install',
            1 => 'module_filter.js',
            2 => 'module_filter.module',
            3 => 'module_filter.admin.inc',
            4 => 'module_filter.theme.inc',
            5 => 'css/module_filter.css',
            6 => 'css/module_filter_tab.css',
            7 => 'js/module_filter.js',
            8 => 'js/module_filter_tab.js',
          ),
          'configure' => 'admin/config/user-interface/modulefilter',
          'version' => '7.x-1.8',
          'project' => 'module_filter',
          'datestamp' => '1375995220',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'project' => 'module_filter',
        'version' => '7.x-1.8',
      ),
      'mongodb_watchdog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_watchdog/mongodb_watchdog.module',
        'basename' => 'mongodb_watchdog.module',
        'name' => 'mongodb_watchdog',
        'info' => 
        array (
          'name' => 'MongoDB watchdog',
          'description' => 'A watchdog implementation using MongoDB',
          'package' => 'MongoDB',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mongodb',
          ),
          'files' => 
          array (
            0 => 'mongodb_watchdog.test',
          ),
          'version' => '7.x-1.0-rc2',
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_block' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_block/mongodb_block.module',
        'basename' => 'mongodb_block.module',
        'name' => 'mongodb_block',
        'info' => 
        array (
          'name' => 'MongoDB Block',
          'description' => 'MongoDB based block system',
          'package' => 'MongoDB',
          'version' => '7.x-1.0-rc2',
          'dependencies' => 
          array (
            0 => 'mongodb',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mongodb_block.module',
            1 => 'mongodb_block.install',
          ),
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_queue' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_queue/mongodb_queue.module',
        'basename' => 'mongodb_queue.module',
        'name' => 'mongodb_queue',
        'info' => 
        array (
          'name' => 'MongoDB Queue',
          'description' => 'Integration between MongoDB and DrupalQueueInterface.',
          'package' => 'MongoDB',
          'version' => '7.x-1.0-rc2',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mongodb_queue.module',
            1 => 'mongodb_queue.inc',
          ),
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_field_storage' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_field_storage/mongodb_field_storage.module',
        'basename' => 'mongodb_field_storage.module',
        'name' => 'mongodb_field_storage',
        'info' => 
        array (
          'name' => 'MongoDB Storage',
          'description' => 'Store Field content on MongoDB.',
          'package' => 'MongoDB',
          'version' => '7.x-1.0-rc2',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mongodb',
          ),
          'files' => 
          array (
            0 => 'mongodb_field_storage.module',
            1 => 'mongodb_field_storage.test',
          ),
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_block_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_block_ui/mongodb_block_ui.module',
        'basename' => 'mongodb_block_ui.module',
        'name' => 'mongodb_block_ui',
        'info' => 
        array (
          'name' => 'MongoDB Block UI',
          'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
          'package' => 'MongoDB',
          'version' => '7.x-1.0-rc2',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mongodb_block',
          ),
          'files' => 
          array (
            0 => 'mongodb_block_ui.module',
            1 => 'mongodb_block_ui.admin.inc',
            2 => 'mongodb_block_ui.install',
          ),
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_session' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_session/mongodb_session.module',
        'basename' => 'mongodb_session.module',
        'name' => 'mongodb_session',
        'info' => 
        array (
          'name' => 'MongoDB Session',
          'description' => 'Store session information in MongoDB',
          'package' => 'MongoDB',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mongodb_session.module',
            1 => 'mongodb_session.inc',
            2 => 'mongodb_session.test',
          ),
          'version' => '7.x-1.0-rc2',
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_cache' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_cache/mongodb_cache.module',
        'basename' => 'mongodb_cache.module',
        'name' => 'mongodb_cache',
        'info' => 
        array (
          'name' => 'MongoDB Cache',
          'description' => 'Integration between MongoDB as Cache.',
          'package' => 'MongoDB',
          'version' => '7.x-1.0-rc2',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mongodb_cache.module',
            1 => 'mongodb_cache.inc',
          ),
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb_migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb_migrate/mongodb_migrate.module',
        'basename' => 'mongodb_migrate.module',
        'name' => 'mongodb_migrate',
        'info' => 
        array (
          'name' => 'MongoDB Migrate',
          'description' => 'Migrate fields from MySQL to MongoDB',
          'package' => 'MongoDB',
          'core' => '7.x',
          'requirements' => 
          array (
            0 => 'entity',
            1 => 'mongodb_field_storage',
          ),
          'version' => '7.x-1.0-rc2',
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'mongodb' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/mongodb/mongodb.module',
        'basename' => 'mongodb.module',
        'name' => 'mongodb',
        'info' => 
        array (
          'name' => 'MongoDB',
          'description' => 'Integration between Drupal and MongoDB.',
          'package' => 'MongoDB',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mongodb.module',
            1 => 'mongodb.install',
          ),
          'version' => '7.x-1.0-rc2',
          'project' => 'mongodb',
          'datestamp' => '1345275440',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'mongodb',
        'version' => '7.x-1.0-rc2',
      ),
      'entityreference_feeds' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entityreference_feeds/entityreference_feeds.module',
        'basename' => 'entityreference_feeds.module',
        'name' => 'entityreference_feeds',
        'info' => 
        array (
          'name' => 'Entity Reference feeds',
          'description' => 'Provides feeds integration for entityreference for mapping directly to properties of referenced entities',
          'core' => '7.x',
          'package' => 'Feeds',
          'dependencies' => 
          array (
            0 => 'feeds',
            1 => 'entityreference',
            2 => 'entity',
          ),
          'configure' => 'admin/config/content/entityreference_feeds',
          'version' => '7.x-1.0-beta1',
          'project' => 'entityreference_feeds',
          'datestamp' => '1363780517',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entityreference_feeds',
        'version' => '7.x-1.0-beta1',
      ),
      'globalredirect' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/globalredirect/globalredirect.module',
        'basename' => 'globalredirect.module',
        'name' => 'globalredirect',
        'info' => 
        array (
          'name' => 'Global Redirect',
          'description' => 'Searches for an alias of the current URL and 301 redirects if found. Stops duplicate content arising when path module is enabled.',
          'package' => 'Path management',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'globalredirect.test',
          ),
          'configure' => 'admin/config/system/globalredirect',
          'version' => '7.x-1.5',
          'project' => 'globalredirect',
          'datestamp' => '1339748779',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'project' => 'globalredirect',
        'version' => '7.x-1.5',
      ),
      'charts_google' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/charts/modules/charts_google/charts_google.module',
        'basename' => 'charts_google.module',
        'name' => 'charts_google',
        'info' => 
        array (
          'name' => 'Google Charts',
          'description' => 'Charts module integration with Google Charts.',
          'package' => 'Charts',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'charts',
          ),
          'version' => '7.x-2.0-beta5',
          'project' => 'charts',
          'datestamp' => '1375159271',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'charts',
        'version' => '7.x-2.0-beta5',
      ),
      'charts_highcharts' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/charts/modules/charts_highcharts/charts_highcharts.module',
        'basename' => 'charts_highcharts.module',
        'name' => 'charts_highcharts',
        'info' => 
        array (
          'name' => 'Highcharts',
          'description' => 'Charts module integration with Highcharts library.',
          'package' => 'Charts',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'libraries',
          ),
          'version' => '7.x-2.0-beta5',
          'project' => 'charts',
          'datestamp' => '1375159271',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'charts',
        'version' => '7.x-2.0-beta5',
      ),
      'charts' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/charts/charts.module',
        'basename' => 'charts.module',
        'name' => 'charts',
        'info' => 
        array (
          'name' => 'Charts',
          'description' => 'A charting API for Drupal that provides chart elements and integration with Views.',
          'package' => 'Charts',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'views/charts_plugin_style_chart.inc',
            1 => 'views/charts_plugin_display_chart.inc',
          ),
          'version' => '7.x-2.0-beta5',
          'project' => 'charts',
          'datestamp' => '1375159271',
          'dependencies' => 
          array (
          ),
        ),
        'schema_version' => 0,
        'project' => 'charts',
        'version' => '7.x-2.0-beta5',
      ),
      'better_formats' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/better_formats/better_formats.module',
        'basename' => 'better_formats.module',
        'name' => 'better_formats',
        'info' => 
        array (
          'name' => 'Better Formats',
          'description' => 'Enhances the core input format system by managing input format defaults and settings.',
          'core' => '7.x',
          'configure' => 'admin/config/content/formats',
          'version' => '7.x-1.0-beta1',
          'project' => 'better_formats',
          'datestamp' => '1343262404',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'better_formats',
        'version' => '7.x-1.0-beta1',
      ),
      'clone' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/node_clone/clone.module',
        'basename' => 'clone.module',
        'name' => 'clone',
        'info' => 
        array (
          'name' => 'Node clone',
          'description' => 'Allows users to clone (copy then edit) an existing node.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'views/views_handler_field_node_link_clone.inc',
          ),
          'configure' => 'admin/config/content/clone',
          'version' => '7.x-1.0-rc1',
          'project' => 'node_clone',
          'datestamp' => '1344129444',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'node_clone',
        'version' => '7.x-1.0-rc1',
      ),
      'views_highcharts' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/highcharts/views_highcharts.module',
        'basename' => 'views_highcharts.module',
        'name' => 'views_highcharts',
        'info' => 
        array (
          'name' => 'Highcharts',
          'package' => 'Views',
          'description' => 'Views formatter to produce highchart',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
            1 => 'uuid',
            2 => 'libraries',
          ),
          'files' => 
          array (
            0 => 'views_highcharts_plugin_style_highcharts.inc',
          ),
          'version' => '7.x-1.0-alpha6',
          'project' => 'highcharts',
          'datestamp' => '1332724847',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'highcharts',
        'version' => '7.x-1.0-alpha6',
      ),
      'chosen' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/chosen/chosen.module',
        'basename' => 'chosen.module',
        'name' => 'chosen',
        'info' => 
        array (
          'name' => 'Chosen',
          'description' => 'Makes select elements more user-friendly using <a href="http://harvesthq.github.com/chosen/">Chosen</a>.',
          'configure' => 'admin/config/user-interface/chosen',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'jquery_update',
          ),
          'version' => '7.x-2.0-alpha4',
          'project' => 'chosen',
          'datestamp' => '1379598967',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'project' => 'chosen',
        'version' => '7.x-2.0-alpha4',
      ),
      'libraries' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/libraries/libraries.module',
        'basename' => 'libraries.module',
        'name' => 'libraries',
        'info' => 
        array (
          'name' => 'Libraries',
          'description' => 'Allows version-dependent and shared usage of external libraries.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/libraries.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'libraries',
          'datestamp' => '1362848412',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'project' => 'libraries',
        'version' => '7.x-2.1',
      ),
      'help_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/advanced_help/help_example/help_example.module',
        'basename' => 'help_example.module',
        'name' => 'help_example',
        'info' => 
        array (
          'name' => 'Advanced help example',
          'description' => 'A example help module to demonstrate the advanced help module.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advanced_help',
          ),
          'files' => 
          array (
            0 => 'help_example.module',
          ),
          'version' => '7.x-1.0',
          'project' => 'advanced_help',
          'datestamp' => '1321022730',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'advanced_help',
        'version' => '7.x-1.0',
      ),
      'advanced_help' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/advanced_help/advanced_help.module',
        'basename' => 'advanced_help.module',
        'name' => 'advanced_help',
        'info' => 
        array (
          'name' => 'Advanced help',
          'description' => 'Allow advanced help and documentation.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'advanced_help.module',
            1 => 'advanced_help.install',
          ),
          'version' => '7.x-1.0',
          'project' => 'advanced_help',
          'datestamp' => '1321022730',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'advanced_help',
        'version' => '7.x-1.0',
      ),
      'empty_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/empty_fields/empty_fields.module',
        'basename' => 'empty_fields.module',
        'name' => 'empty_fields',
        'info' => 
        array (
          'name' => 'Empty Fields',
          'description' => 'Provides options for displaying empty fields.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'plugins/empty_fields_handler.inc',
            1 => 'plugins/empty_fields_handler_text.inc',
          ),
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'field_formatter_settings',
          ),
          'version' => '7.x-2.0-rc1',
          'project' => 'empty_fields',
          'datestamp' => '1352905605',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'empty_fields',
        'version' => '7.x-2.0-rc1',
      ),
      'feeds_import' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds/feeds_import/feeds_import.module',
        'basename' => 'feeds_import.module',
        'name' => 'feeds_import',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'feeds',
          ),
          'description' => 'An example of a node importer and a user importer.',
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'feeds:feeds_importer_default:1',
            ),
            'feeds_importer' => 
            array (
              0 => 'node',
              1 => 'user',
            ),
          ),
          'files' => 
          array (
            0 => 'feeds_import.test',
          ),
          'name' => 'Feeds Import',
          'package' => 'Feeds',
          'php' => '5.2.4',
          'version' => '7.x-2.0-alpha8',
          'project' => 'feeds',
          'datestamp' => '1366671911',
        ),
        'schema_version' => 0,
        'project' => 'feeds',
        'version' => '7.x-2.0-alpha8',
      ),
      'feeds_news' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds/feeds_news/feeds_news.module',
        'basename' => 'feeds_news.module',
        'name' => 'feeds_news',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'feeds',
            2 => 'views',
          ),
          'description' => 'A news aggregator built with feeds, creates nodes from imported feed items. With OPML import.',
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'feeds:feeds_importer_default:1',
              1 => 'views:views_default:3.0',
            ),
            'feeds_importer' => 
            array (
              0 => 'feed',
              1 => 'opml',
            ),
            'field' => 
            array (
              0 => 'node-feed_item-field_feed_item_description',
            ),
            'node' => 
            array (
              0 => 'feed',
              1 => 'feed_item',
            ),
            'views_view' => 
            array (
              0 => 'feeds_defaults_feed_items',
            ),
          ),
          'files' => 
          array (
            0 => 'feeds_news.module',
            1 => 'feeds_news.test',
          ),
          'name' => 'Feeds News',
          'package' => 'Feeds',
          'php' => '5.2.4',
          'version' => '7.x-2.0-alpha8',
          'project' => 'feeds',
          'datestamp' => '1366671911',
        ),
        'schema_version' => 0,
        'project' => 'feeds',
        'version' => '7.x-2.0-alpha8',
      ),
      'feeds_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds/feeds_ui/feeds_ui.module',
        'basename' => 'feeds_ui.module',
        'name' => 'feeds_ui',
        'info' => 
        array (
          'name' => 'Feeds Admin UI',
          'description' => 'Administrative UI for Feeds module.',
          'package' => 'Feeds',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'feeds',
          ),
          'configure' => 'admin/structure/feeds',
          'files' => 
          array (
            0 => 'feeds_ui.test',
          ),
          'version' => '7.x-2.0-alpha8',
          'project' => 'feeds',
          'datestamp' => '1366671911',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'feeds',
        'version' => '7.x-2.0-alpha8',
      ),
      'feeds' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds/feeds.module',
        'basename' => 'feeds.module',
        'name' => 'feeds',
        'info' => 
        array (
          'name' => 'Feeds',
          'description' => 'Aggregates RSS/Atom/RDF feeds, imports CSV files and more.',
          'package' => 'Feeds',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'job_scheduler',
          ),
          'files' => 
          array (
            0 => 'includes/FeedsConfigurable.inc',
            1 => 'includes/FeedsImporter.inc',
            2 => 'includes/FeedsSource.inc',
            3 => 'libraries/ParserCSV.inc',
            4 => 'libraries/http_request.inc',
            5 => 'libraries/PuSHSubscriber.inc',
            6 => 'plugins/FeedsCSVParser.inc',
            7 => 'plugins/FeedsFetcher.inc',
            8 => 'plugins/FeedsFileFetcher.inc',
            9 => 'plugins/FeedsHTTPFetcher.inc',
            10 => 'plugins/FeedsNodeProcessor.inc',
            11 => 'plugins/FeedsOPMLParser.inc',
            12 => 'plugins/FeedsParser.inc',
            13 => 'plugins/FeedsPlugin.inc',
            14 => 'plugins/FeedsProcessor.inc',
            15 => 'plugins/FeedsSimplePieParser.inc',
            16 => 'plugins/FeedsSitemapParser.inc',
            17 => 'plugins/FeedsSyndicationParser.inc',
            18 => 'plugins/FeedsTermProcessor.inc',
            19 => 'plugins/FeedsUserProcessor.inc',
            20 => 'tests/feeds.test',
            21 => 'tests/feeds_date_time.test',
            22 => 'tests/feeds_mapper_date.test',
            23 => 'tests/feeds_mapper_date_multiple.test',
            24 => 'tests/feeds_mapper_field.test',
            25 => 'tests/feeds_mapper_file.test',
            26 => 'tests/feeds_mapper_path.test',
            27 => 'tests/feeds_mapper_profile.test',
            28 => 'tests/feeds_mapper.test',
            29 => 'tests/feeds_mapper_config.test',
            30 => 'tests/feeds_fetcher_file.test',
            31 => 'tests/feeds_processor_node.test',
            32 => 'tests/feeds_processor_term.test',
            33 => 'tests/feeds_processor_user.test',
            34 => 'tests/feeds_scheduler.test',
            35 => 'tests/feeds_mapper_link.test',
            36 => 'tests/feeds_mapper_taxonomy.test',
            37 => 'tests/parser_csv.test',
            38 => 'views/feeds_views_handler_argument_importer_id.inc',
            39 => 'views/feeds_views_handler_field_importer_name.inc',
            40 => 'views/feeds_views_handler_field_log_message.inc',
            41 => 'views/feeds_views_handler_field_severity.inc',
            42 => 'views/feeds_views_handler_field_source.inc',
            43 => 'views/feeds_views_handler_filter_severity.inc',
          ),
          'version' => '7.x-2.0-alpha8',
          'project' => 'feeds',
          'datestamp' => '1366671911',
          'php' => '5.2.4',
        ),
        'schema_version' => '7208',
        'project' => 'feeds',
        'version' => '7.x-2.0-alpha8',
      ),
      'conditional_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/conditional_fields/conditional_fields.module',
        'basename' => 'conditional_fields.module',
        'name' => 'conditional_fields',
        'info' => 
        array (
          'name' => 'Conditional Fields',
          'description' => 'Define dependencies between fields based on their states and values.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'configure' => 'admin/structure/dependencies',
          'version' => '7.x-3.x-dev',
          'project' => 'conditional_fields',
          'datestamp' => '1352724212',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'conditional_fields',
        'version' => '7.x-3.x-dev',
      ),
      'geofield_map' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/geofield/modules/geofield_map/geofield_map.module',
        'basename' => 'geofield_map.module',
        'name' => 'geofield_map',
        'info' => 
        array (
          'name' => 'Geofield Map',
          'description' => 'Provides a basic mapping interface for Geofield.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geofield',
          ),
          'files' => 
          array (
            0 => 'includes/geofield_map.views.inc',
            1 => 'includes/geofield_map_plugin_style_map.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'geofield',
          'datestamp' => '1338941478',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'geofield',
        'version' => '7.x-1.1',
      ),
      'geofield' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/geofield/geofield.module',
        'basename' => 'geofield.module',
        'name' => 'geofield',
        'info' => 
        array (
          'name' => 'Geofield',
          'description' => 'Stores geographic and location data (points, lines, and polygons).',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geophp',
          ),
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'geofield.module',
            1 => 'geofield.install',
            2 => 'geofield.widgets.inc',
            3 => 'geofield.formatters.inc',
            4 => 'geofield.openlayers.inc',
            5 => 'geofield.feeds.inc',
            6 => 'geofield.test',
          ),
          'version' => '7.x-1.1',
          'project' => 'geofield',
          'datestamp' => '1338941478',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'geofield',
        'version' => '7.x-1.1',
      ),
      'flog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/flog/flog.module',
        'basename' => 'flog.module',
        'name' => 'flog',
        'info' => 
        array (
          'name' => 'File logger',
          'description' => 'Allows developers to dump variables to a file for easy tailing during development.',
          'core' => '7.x',
          'package' => 'Development',
          'configure' => 'admin/config/development/flog',
          'files' => 
          array (
            0 => 'flog.module',
          ),
          'version' => '7.x-1.1',
          'project' => 'flog',
          'datestamp' => '1344275549',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'flog',
        'version' => '7.x-1.1',
      ),
      'options_element' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/options_element/options_element.module',
        'basename' => 'options_element.module',
        'name' => 'options_element',
        'info' => 
        array (
          'name' => 'Options element',
          'description' => 'A custom form element for entering the options in select lists, radios, or checkboxes.',
          'core' => '7.x',
          'package' => 'User interface',
          'version' => '7.x-1.9',
          'project' => 'options_element',
          'datestamp' => '1367014511',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'options_element',
        'version' => '7.x-1.9',
      ),
      'actions_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/views_bulk_operations/actions_permissions.module',
        'basename' => 'actions_permissions.module',
        'name' => 'actions_permissions',
        'info' => 
        array (
          'name' => 'Actions permissions (VBO)',
          'description' => 'Provides permission-based access control for actions. Used by Views Bulk Operations.',
          'package' => 'Administration',
          'core' => '7.x',
          'version' => '7.x-3.1',
          'project' => 'views_bulk_operations',
          'datestamp' => '1354500015',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'views_bulk_operations',
        'version' => '7.x-3.1',
      ),
      'views_bulk_operations' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/views_bulk_operations/views_bulk_operations.module',
        'basename' => 'views_bulk_operations.module',
        'name' => 'views_bulk_operations',
        'info' => 
        array (
          'name' => 'Views Bulk Operations',
          'description' => 'Provides a way of selecting multiple rows and applying operations to them.',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'views',
          ),
          'package' => 'Views',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/operation_types/base.class.php',
            1 => 'views/views_bulk_operations_handler_field_operations.inc',
          ),
          'version' => '7.x-3.1',
          'project' => 'views_bulk_operations',
          'datestamp' => '1354500015',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'views_bulk_operations',
        'version' => '7.x-3.1',
      ),
      'token' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/token/token.module',
        'basename' => 'token.module',
        'name' => 'token',
        'info' => 
        array (
          'name' => 'Token',
          'description' => 'Provides a user interface for the Token API and some missing core tokens.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'token.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'token',
          'datestamp' => '1361665026',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'token',
        'version' => '7.x-1.5',
      ),
      'migrate_example_oracle' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate/migrate_example/migrate_example_oracle/migrate_example_oracle.module',
        'basename' => 'migrate_example_oracle.module',
        'name' => 'migrate_example_oracle',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'image',
            2 => 'migrate',
          ),
          'description' => 'Content type supporting example of Oracle migration',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-migrate_example_oracle-body',
              1 => 'node-migrate_example_oracle-field_mainimage',
            ),
            'node' => 
            array (
              0 => 'migrate_example_oracle',
            ),
          ),
          'files' => 
          array (
            0 => 'migrate_example_oracle.migrate.inc',
          ),
          'name' => 'Migrate example - Oracle',
          'package' => 'Migrate Examples',
          'project' => 'migrate',
          'version' => '7.x-2.5',
          'datestamp' => '1352299007',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'migrate',
        'version' => '7.x-2.5',
      ),
      'migrate_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate/migrate_example/migrate_example.module',
        'basename' => 'migrate_example.module',
        'name' => 'migrate_example',
        'info' => 
        array (
          'name' => 'Migrate Example',
          'description' => 'Example migration data.',
          'package' => 'Development',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'image',
            2 => 'comment',
            3 => 'migrate',
            4 => 'list',
            5 => 'number',
          ),
          'files' => 
          array (
            0 => 'migrate_example.module',
            1 => 'beer.inc',
            2 => 'wine.inc',
          ),
          'version' => '7.x-2.5',
          'project' => 'migrate',
          'datestamp' => '1352299007',
          'php' => '5.2.4',
        ),
        'schema_version' => '7007',
        'project' => 'migrate',
        'version' => '7.x-2.5',
      ),
      'migrate_example_baseball' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate/migrate_example_baseball/migrate_example_baseball.module',
        'basename' => 'migrate_example_baseball.module',
        'name' => 'migrate_example_baseball',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'migrate',
            2 => 'number',
          ),
          'description' => 'Import baseball box scores.',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-migrate_example_baseball-body',
              1 => 'node-migrate_example_baseball-field_attendance',
              2 => 'node-migrate_example_baseball-field_duration',
              3 => 'node-migrate_example_baseball-field_home_batters',
              4 => 'node-migrate_example_baseball-field_home_game_number',
              5 => 'node-migrate_example_baseball-field_home_pitcher',
              6 => 'node-migrate_example_baseball-field_home_score',
              7 => 'node-migrate_example_baseball-field_home_team',
              8 => 'node-migrate_example_baseball-field_outs',
              9 => 'node-migrate_example_baseball-field_park',
              10 => 'node-migrate_example_baseball-field_start_date',
              11 => 'node-migrate_example_baseball-field_visiting_batters',
              12 => 'node-migrate_example_baseball-field_visiting_pitcher',
              13 => 'node-migrate_example_baseball-field_visiting_score',
              14 => 'node-migrate_example_baseball-field_visiting_team',
            ),
            'node' => 
            array (
              0 => 'migrate_example_baseball',
            ),
          ),
          'files' => 
          array (
            0 => 'migrate_example_baseball.migrate.inc',
          ),
          'name' => 'migrate_example_baseball',
          'package' => 'Migrate Examples',
          'php' => '5.2.4',
          'version' => '7.x-2.5',
          'project' => 'migrate',
          'datestamp' => '1352299007',
        ),
        'schema_version' => '7201',
        'project' => 'migrate',
        'version' => '7.x-2.5',
      ),
      'migrate_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate/migrate_ui/migrate_ui.module',
        'basename' => 'migrate_ui.module',
        'name' => 'migrate_ui',
        'info' => 
        array (
          'name' => 'Migrate UI',
          'description' => 'UI for managing migration processes',
          'package' => 'Development',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'migrate',
          ),
          'files' => 
          array (
            0 => 'migrate_ui.module',
          ),
          'version' => '7.x-2.5',
          'project' => 'migrate',
          'datestamp' => '1352299007',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'migrate',
        'version' => '7.x-2.5',
      ),
      'migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate/migrate.module',
        'basename' => 'migrate.module',
        'name' => 'migrate',
        'info' => 
        array (
          'name' => 'Migrate',
          'description' => 'Import content from external sources',
          'package' => 'Development',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/base.inc',
            1 => 'includes/field_mapping.inc',
            2 => 'includes/migration.inc',
            3 => 'includes/destination.inc',
            4 => 'includes/exception.inc',
            5 => 'includes/group.inc',
            6 => 'includes/handler.inc',
            7 => 'includes/map.inc',
            8 => 'includes/source.inc',
            9 => 'includes/team.inc',
            10 => 'migrate.mail.inc',
            11 => 'plugins/destinations/entity.inc',
            12 => 'plugins/destinations/term.inc',
            13 => 'plugins/destinations/user.inc',
            14 => 'plugins/destinations/node.inc',
            15 => 'plugins/destinations/comment.inc',
            16 => 'plugins/destinations/file.inc',
            17 => 'plugins/destinations/path.inc',
            18 => 'plugins/destinations/fields.inc',
            19 => 'plugins/destinations/poll.inc',
            20 => 'plugins/destinations/table.inc',
            21 => 'plugins/destinations/table_copy.inc',
            22 => 'plugins/destinations/menu.inc',
            23 => 'plugins/destinations/menu_links.inc',
            24 => 'plugins/destinations/statistics.inc',
            25 => 'plugins/sources/csv.inc',
            26 => 'plugins/sources/files.inc',
            27 => 'plugins/sources/json.inc',
            28 => 'plugins/sources/list.inc',
            29 => 'plugins/sources/multiitems.inc',
            30 => 'plugins/sources/sql.inc',
            31 => 'plugins/sources/sqlmap.inc',
            32 => 'plugins/sources/mssql.inc',
            33 => 'plugins/sources/oracle.inc',
            34 => 'plugins/sources/xml.inc',
            35 => 'tests/import/options.test',
            36 => 'tests/plugins/destinations/comment.test',
            37 => 'tests/plugins/destinations/node.test',
            38 => 'tests/plugins/destinations/table.test',
            39 => 'tests/plugins/destinations/term.test',
            40 => 'tests/plugins/destinations/user.test',
            41 => 'tests/plugins/sources/xml.test',
          ),
          'version' => '7.x-2.5',
          'project' => 'migrate',
          'datestamp' => '1352299007',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'project' => 'migrate',
        'version' => '7.x-2.5',
      ),
      'entityreference_behavior_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entityreference/examples/entityreference_behavior_example/entityreference_behavior_example.module',
        'basename' => 'entityreference_behavior_example.module',
        'name' => 'entityreference_behavior_example',
        'info' => 
        array (
          'name' => 'Entity Reference Behavior Example',
          'description' => 'Provides some example code for implementing Entity Reference behaviors.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entityreference',
          ),
          'version' => '7.x-1.0',
          'project' => 'entityreference',
          'datestamp' => '1353230808',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entityreference',
        'version' => '7.x-1.0',
      ),
      'entityreference' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entityreference/entityreference.module',
        'basename' => 'entityreference.module',
        'name' => 'entityreference',
        'info' => 
        array (
          'name' => 'Entity Reference',
          'description' => 'Provides a field that can reference other entities.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'entityreference.migrate.inc',
            1 => 'plugins/selection/abstract.inc',
            2 => 'plugins/selection/views.inc',
            3 => 'plugins/behavior/abstract.inc',
            4 => 'views/entityreference_plugin_display.inc',
            5 => 'views/entityreference_plugin_style.inc',
            6 => 'views/entityreference_plugin_row_fields.inc',
            7 => 'tests/entityreference.handlers.test',
            8 => 'tests/entityreference.taxonomy.test',
            9 => 'tests/entityreference.admin.test',
          ),
          'version' => '7.x-1.0',
          'project' => 'entityreference',
          'datestamp' => '1353230808',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'entityreference',
        'version' => '7.x-1.0',
      ),
      'devel_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/devel/devel_generate/devel_generate.module',
        'basename' => 'devel_generate.module',
        'name' => 'devel_generate',
        'info' => 
        array (
          'name' => 'Devel generate',
          'description' => 'Generate dummy users, nodes, and taxonomy terms.',
          'package' => 'Development',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'devel',
          ),
          'tags' => 
          array (
            0 => 'developer',
          ),
          'configure' => 'admin/config/development/generate',
          'version' => '7.x-1.3',
          'project' => 'devel',
          'datestamp' => '1338940281',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'devel',
        'version' => '7.x-1.3',
      ),
      'devel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/devel/devel.module',
        'basename' => 'devel.module',
        'name' => 'devel',
        'info' => 
        array (
          'name' => 'Devel',
          'description' => 'Various blocks, pages, and functions for developers.',
          'package' => 'Development',
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'files' => 
          array (
            0 => 'devel.test',
            1 => 'devel.mail.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'devel',
          'datestamp' => '1338940281',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'devel',
        'version' => '7.x-1.3',
      ),
      'devel_node_access' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/devel/devel_node_access.module',
        'basename' => 'devel_node_access.module',
        'name' => 'devel_node_access',
        'info' => 
        array (
          'name' => 'Devel node access',
          'description' => 'Developer blocks and page illustrating relevant node_access records.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'menu',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'version' => '7.x-1.3',
          'project' => 'devel',
          'datestamp' => '1338940281',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'devel',
        'version' => '7.x-1.3',
      ),
      'auto_nodetitle' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/auto_nodetitle/auto_nodetitle.module',
        'basename' => 'auto_nodetitle.module',
        'name' => 'auto_nodetitle',
        'info' => 
        array (
          'name' => 'Automatic Nodetitles',
          'description' => 'Allows hiding of the content title field and automatic title creation.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'auto_nodetitle.install',
            1 => 'auto_nodetitle.module',
            2 => 'auto_nodetitle.js',
          ),
          'version' => '7.x-1.0',
          'project' => 'auto_nodetitle',
          'datestamp' => '1307449915',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '1',
        'project' => 'auto_nodetitle',
        'version' => '7.x-1.0',
      ),
      'quicktabs_tabstyles' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/quicktabs/quicktabs_tabstyles/quicktabs_tabstyles.module',
        'basename' => 'quicktabs_tabstyles.module',
        'name' => 'quicktabs_tabstyles',
        'info' => 
        array (
          'name' => 'Quicktabs Styles',
          'description' => 'Adds predefined tab styles to choose from per Quicktabs instance.',
          'core' => '7.x',
          'configure' => 'admin/structure/quicktabs/styles',
          'dependencies' => 
          array (
            0 => 'quicktabs',
          ),
          'version' => '7.x-3.4',
          'project' => 'quicktabs',
          'datestamp' => '1332980461',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'quicktabs',
        'version' => '7.x-3.4',
      ),
      'quicktabs' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/quicktabs/quicktabs.module',
        'basename' => 'quicktabs.module',
        'name' => 'quicktabs',
        'info' => 
        array (
          'name' => 'Quicktabs',
          'description' => 'Render content with tabs and other display styles',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'quicktabs.module',
            1 => 'quicktabs.classes.inc',
            2 => 'includes/quicktabs_style_plugin.inc',
            3 => 'tests/quicktabs.test',
          ),
          'configure' => 'admin/structure/quicktabs',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'version' => '7.x-3.4',
          'project' => 'quicktabs',
          'datestamp' => '1332980461',
          'php' => '5.2.4',
        ),
        'schema_version' => '7302',
        'project' => 'quicktabs',
        'version' => '7.x-3.4',
      ),
      'geofield_gmap' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/geofield_gmap/geofield_gmap.module',
        'basename' => 'geofield_gmap.module',
        'name' => 'geofield_gmap',
        'info' => 
        array (
          'name' => 'Geofield Gmap',
          'description' => 'Google Map widget and formatter for geofield.',
          'dependencies' => 
          array (
            0 => 'geofield',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'geofield_gmap.admin.inc',
            1 => 'geofield_gmap.install',
            2 => 'geofield_gmap.module',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'geofield_gmap.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'geofield_gmap.js',
          ),
          'configure' => 'admin/config/content/geofield_gmap',
          'version' => '7.x-1.x-dev',
          'project' => 'geofield_gmap',
          'datestamp' => '1354928447',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'geofield_gmap',
        'version' => '7.x-1.x-dev',
      ),
      'autocomplete_deluxe' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/autocomplete_deluxe/autocomplete_deluxe.module',
        'basename' => 'autocomplete_deluxe.module',
        'name' => 'autocomplete_deluxe',
        'info' => 
        array (
          'name' => 'Autocomplete Deluxe',
          'description' => 'Enhanced autocomplete using Jquery UI autocomplete.',
          'package' => 'User interface',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'autocomplete_deluxe.module',
          ),
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'version' => '7.x-2.0-beta3',
          'project' => 'autocomplete_deluxe',
          'datestamp' => '1375695669',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'autocomplete_deluxe',
        'version' => '7.x-2.0-beta3',
      ),
      'field_formatter_settings' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_formatter_settings/field_formatter_settings.module',
        'basename' => 'field_formatter_settings.module',
        'name' => 'field_formatter_settings',
        'info' => 
        array (
          'name' => 'Field formatter settings API',
          'description' => 'Provides missing alter hooks for field formatter settings and summaries',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field_ui',
          ),
          'files' => 
          array (
            0 => 'field_formatter_settings.module',
          ),
          'version' => '7.x-1.0',
          'project' => 'field_formatter_settings',
          'datestamp' => '1345588632',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'field_formatter_settings',
        'version' => '7.x-1.0',
      ),
      'profiler_builder' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/profiler_builder/profiler_builder.module',
        'basename' => 'profiler_builder.module',
        'name' => 'profiler_builder',
        'info' => 
        array (
          'name' => 'Profiler Builder',
          'description' => 'Turn this site into a profiler distribution fast!',
          'package' => 'Development',
          'core' => '7.x',
          'configure' => 'admin/config/development/profiler_builder',
          'version' => '7.x-1.0-rc4',
          'project' => 'profiler_builder',
          'datestamp' => '1364330717',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'profiler_builder',
        'version' => '7.x-1.0-rc4',
      ),
      'argfilters' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/views_arguments_in_filters/argfilters.module',
        'basename' => 'argfilters.module',
        'name' => 'argfilters',
        'info' => 
        array (
          'name' => 'Views arguments in filters',
          'description' => 'Allows you to use argument values in filters, thereby leveraging functionality such as operators.',
          'package' => 'Experimental',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'entitycache' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entitycache/entitycache.module',
        'basename' => 'entitycache.module',
        'name' => 'entitycache',
        'info' => 
        array (
          'name' => 'Entity cache',
          'description' => 'Provides caching for core entities including nodes and taxonomy terms.',
          'core' => '7.x',
          'package' => 'Performance and scalability',
          'files' => 
          array (
            0 => 'entitycache.module',
            1 => 'entitycache.comment.inc',
            2 => 'entitycache.taxonomy.inc',
            3 => 'entitycache.test',
          ),
          'version' => '7.x-1.1',
          'project' => 'entitycache',
          'datestamp' => '1315901203',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'entitycache',
        'version' => '7.x-1.1',
      ),
      'field_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_permissions/field_permissions.module',
        'basename' => 'field_permissions.module',
        'name' => 'field_permissions',
        'info' => 
        array (
          'name' => 'Field Permissions',
          'description' => 'Set field-level permissions to create, update or view fields.',
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_permissions.module',
            1 => 'field_permissions.admin.inc',
            2 => 'field_permissions.test',
          ),
          'configure' => 'admin/reports/fields/permissions',
          'version' => '7.x-1.0-beta2',
          'project' => 'field_permissions',
          'datestamp' => '1327510549',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'field_permissions',
        'version' => '7.x-1.0-beta2',
      ),
      'field_analytics_highcharts' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_analytics/highcharts/field_analytics_highcharts.module',
        'basename' => 'field_analytics_highcharts.module',
        'name' => 'field_analytics_highcharts',
        'info' => 
        array (
          'name' => 'Field analytics highcharts',
          'description' => 'Integrates Field analytics with Highcharts.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field_analytics',
            1 => 'highcharts',
          ),
          'files' => 
          array (
            0 => 'field_analytics_highcharts.reports.inc',
          ),
          'version' => '7.x-1.x-dev',
          'project' => 'field_analytics',
          'datestamp' => '1380578772',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'field_analytics',
        'version' => '7.x-1.x-dev',
      ),
      'field_analytics' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_analytics/field_analytics.module',
        'basename' => 'field_analytics.module',
        'name' => 'field_analytics',
        'info' => 
        array (
          'name' => 'Field analytics',
          'description' => 'Analyzes entity field statistics.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_analytics.efq.inc',
            1 => 'field_analytics.reports.inc',
          ),
          'version' => '7.x-1.x-dev',
          'project' => 'field_analytics',
          'datestamp' => '1380578772',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'field_analytics',
        'version' => '7.x-1.x-dev',
      ),
      'ds_search' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/modules/ds_search/ds_search.module',
        'basename' => 'ds_search.module',
        'name' => 'ds_search',
        'info' => 
        array (
          'name' => 'Display Suite Search',
          'description' => 'Extend the display options for search results for Drupal Core or Apache Solr.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/search',
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'ds_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/modules/ds_devel/ds_devel.module',
        'basename' => 'ds_devel.module',
        'name' => 'ds_devel',
        'info' => 
        array (
          'name' => 'Display Suite Devel',
          'description' => 'Development functionality for Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
            1 => 'devel',
          ),
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'ds_extras' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/modules/ds_extras/ds_extras.module',
        'basename' => 'ds_extras.module',
        'name' => 'ds_extras',
        'info' => 
        array (
          'name' => 'Display Suite Extras',
          'description' => 'Contains additional features for Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/extras',
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'ds_forms' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/modules/ds_forms/ds_forms.module',
        'basename' => 'ds_forms.module',
        'name' => 'ds_forms',
        'info' => 
        array (
          'name' => 'Display Suite Forms',
          'description' => 'Manage the layout of forms in Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'ds_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/modules/ds_ui/ds_ui.module',
        'basename' => 'ds_ui.module',
        'name' => 'ds_ui',
        'info' => 
        array (
          'name' => 'Display Suite UI',
          'description' => 'User interface for managing fields, view modes and classes.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'ds_format' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/modules/ds_format/ds_format.module',
        'basename' => 'ds_format.module',
        'name' => 'ds_format',
        'info' => 
        array (
          'name' => 'Display Suite Format',
          'description' => 'Provides the Display Suite Code format filter.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/extras',
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'ds' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ds/ds.module',
        'basename' => 'ds.module',
        'name' => 'ds',
        'info' => 
        array (
          'name' => 'Display Suite',
          'description' => 'Extend the display options for every entity type.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'views/views_plugin_ds_entity_view.inc',
            1 => 'views/views_plugin_ds_fields_view.inc',
            2 => 'tests/ds.base.test',
            3 => 'tests/ds.search.test',
            4 => 'tests/ds.entities.test',
            5 => 'tests/ds.exportables.test',
            6 => 'tests/ds.views.test',
            7 => 'tests/ds.forms.test',
          ),
          'configure' => 'admin/structure/ds',
          'version' => '7.x-2.4',
          'project' => 'ds',
          'datestamp' => '1371030953',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'project' => 'ds',
        'version' => '7.x-2.4',
      ),
      'migrate_extras_profile2' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_profile2/migrate_extras_profile2.module',
        'basename' => 'migrate_extras_profile2.module',
        'name' => 'migrate_extras_profile2',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'migrate_extras',
            1 => 'profile2',
          ),
          'description' => 'Examples of migrating into Profile2 entities',
          'files' => 
          array (
            0 => 'migrate_extras_profile2.migrate.inc',
          ),
          'name' => 'Migrate Extras Profile2 Example',
          'package' => 'Migrate Examples',
          'version' => '7.x-2.5',
          'project' => 'migrate_extras',
          'datestamp' => '1352299013',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'migrate_extras',
        'version' => '7.x-2.5',
      ),
      'migrate_extras_pathauto' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_pathauto/migrate_extras_pathauto.module',
        'basename' => 'migrate_extras_pathauto.module',
        'name' => 'migrate_extras_pathauto',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'migrate_extras',
            2 => 'pathauto',
          ),
          'description' => 'Examples of migrating with the Pathauto module',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-migrate_example_pathauto-body',
            ),
            'node' => 
            array (
              0 => 'migrate_example_pathauto',
            ),
          ),
          'files' => 
          array (
            0 => 'migrate_extras_pathauto.migrate.inc',
          ),
          'name' => 'Migrate Extras Pathauto Example',
          'package' => 'Migrate Examples',
          'project' => 'migrate_extras',
          'version' => '7.x-2.5',
          'datestamp' => '1352299013',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'migrate_extras',
        'version' => '7.x-2.5',
      ),
      'migrate_extras_media' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_media/migrate_extras_media.module',
        'basename' => 'migrate_extras_media.module',
        'name' => 'migrate_extras_media',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'file',
            2 => 'media',
            3 => 'media_youtube',
            4 => 'migrate',
            5 => 'migrate_extras',
          ),
          'description' => 'Examples for migrating Media',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-migrate_extras_media_example-body',
              1 => 'node-migrate_extras_media_example-field_document',
              2 => 'node-migrate_extras_media_example-field_media_image',
              3 => 'node-migrate_extras_media_example-field_youtube_video',
            ),
            'node' => 
            array (
              0 => 'migrate_extras_media_example',
            ),
          ),
          'files' => 
          array (
            0 => 'migrate_extras_media.migrate.inc',
          ),
          'name' => 'Migrate Extras Media',
          'package' => 'Migrate Examples',
          'version' => '7.x-2.5',
          'project' => 'migrate_extras',
          'datestamp' => '1352299013',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'migrate_extras',
        'version' => '7.x-2.5',
      ),
      'migrate_extras' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/migrate_extras/migrate_extras.module',
        'basename' => 'migrate_extras.module',
        'name' => 'migrate_extras',
        'info' => 
        array (
          'name' => 'Migrate Extras',
          'description' => 'Adds migrate module integration with contrib modules and other miscellaneous tweaks.',
          'core' => '7.x',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'migrate',
          ),
          'files' => 
          array (
            0 => 'addressfield.inc',
            1 => 'cck_phone.inc',
            2 => 'entity_api.inc',
            3 => 'flag.inc',
            4 => 'geofield.inc',
            5 => 'interval.inc',
            6 => 'media.inc',
            7 => 'name.inc',
            8 => 'pathauto.inc',
            9 => 'privatemsg.inc',
            10 => 'profile2.inc',
            11 => 'rules.inc',
            12 => 'user_relationships.inc',
            13 => 'votingapi.inc',
            14 => 'webform.inc',
            15 => 'tests/pathauto.test',
          ),
          'version' => '7.x-2.5',
          'project' => 'migrate_extras',
          'datestamp' => '1352299013',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'migrate_extras',
        'version' => '7.x-2.5',
      ),
      'nodereference_url' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/nodereference_url/nodereference_url.module',
        'basename' => 'nodereference_url.module',
        'name' => 'nodereference_url',
        'info' => 
        array (
          'name' => 'Node Reference URL Widget',
          'description' => 'Adds an additional widget to the Node Reference field that prepopulates a reference by the URL.',
          'dependencies' => 
          array (
            0 => 'node_reference',
          ),
          'files' => 
          array (
            0 => 'nodereference_url.module',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.12',
          'project' => 'nodereference_url',
          'datestamp' => '1316903507',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'nodereference_url',
        'version' => '7.x-1.12',
      ),
      'field_tools_taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_tools/field_tools_taxonomy/field_tools_taxonomy.module',
        'basename' => 'field_tools_taxonomy.module',
        'name' => 'field_tools_taxonomy',
        'info' => 
        array (
          'name' => 'Field tools taxonomy',
          'description' => 'Allows taxonomy vocabularies to be applied to entities.',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'taxonomy',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_tools_taxonomy.module',
            1 => 'field_tools_taxonomy.admin.inc',
          ),
          'version' => '7.x-1.0-alpha3',
          'project' => 'field_tools',
          'datestamp' => '1353836247',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'field_tools',
        'version' => '7.x-1.0-alpha3',
      ),
      'field_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/field_tools/field_tools.module',
        'basename' => 'field_tools.module',
        'name' => 'field_tools',
        'info' => 
        array (
          'name' => 'Field tools',
          'description' => 'Allows fields to be cloned to other entities.',
          'dependencies' => 
          array (
            0 => 'field_ui',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_tools.module',
            1 => 'field_tools.admin.inc',
          ),
          'version' => '7.x-1.0-alpha3',
          'project' => 'field_tools',
          'datestamp' => '1353836247',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'field_tools',
        'version' => '7.x-1.0-alpha3',
      ),
      'tabtamer' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/tabtamer/tabtamer.module',
        'basename' => 'tabtamer.module',
        'name' => 'tabtamer',
        'info' => 
        array (
          'name' => 'Tab Tamer',
          'description' => 'Gives additional control over what tabs are seen by a user.',
          'package' => 'Other',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tabtamer.install',
            1 => 'tabtamer.module',
          ),
          'configure' => 'admin/config/system/tabtamer',
          'version' => '7.x-1.1',
          'project' => 'tabtamer',
          'datestamp' => '1340140644',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'tabtamer',
        'version' => '7.x-1.1',
      ),
      'addressfield' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/addressfield/addressfield.module',
        'basename' => 'addressfield.module',
        'name' => 'addressfield',
        'info' => 
        array (
          'name' => 'Address Field',
          'description' => 'Manage a flexible address field, implementing the xNAL standard.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'views/addressfield_views_handler_filter_country.inc',
          ),
          'version' => '7.x-1.0-beta3',
          'project' => 'addressfield',
          'datestamp' => '1338304248',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'addressfield',
        'version' => '7.x-1.0-beta3',
      ),
      'strongarm' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/strongarm/strongarm.module',
        'basename' => 'strongarm.module',
        'name' => 'strongarm',
        'info' => 
        array (
          'name' => 'Strongarm',
          'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'strongarm.admin.inc',
            1 => 'strongarm.install',
            2 => 'strongarm.module',
          ),
          'version' => '7.x-2.0',
          'project' => 'strongarm',
          'datestamp' => '1339604214',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'project' => 'strongarm',
        'version' => '7.x-2.0',
      ),
      'entityreference_prepopulate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entityreference_prepopulate/entityreference_prepopulate.module',
        'basename' => 'entityreference_prepopulate.module',
        'name' => 'entityreference_prepopulate',
        'info' => 
        array (
          'name' => 'Entity reference prepopulate',
          'description' => 'Prepopulate entity reference values from URL.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entityreference',
          ),
          'files' => 
          array (
            0 => 'entityreference_prepopulate.test',
          ),
          'version' => '7.x-1.3',
          'project' => 'entityreference_prepopulate',
          'datestamp' => '1366630855',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entityreference_prepopulate',
        'version' => '7.x-1.3',
      ),
      'menu_token' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/menu_token/menu_token.module',
        'basename' => 'menu_token.module',
        'name' => 'menu_token',
        'info' => 
        array (
          'name' => 'Menu Token',
          'description' => 'Provides tokens in menu items (links).',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'menu',
            1 => 'entity',
            2 => 'token',
            3 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'menu_token.test',
          ),
          'configure' => 'admin/config/menu_token',
          'version' => '7.x-1.0-beta4',
          'project' => 'menu_token',
          'datestamp' => '1359157010',
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'menu_token',
        'version' => '7.x-1.0-beta4',
      ),
      'feeds_tamper_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds_tamper/feeds_tamper_ui/feeds_tamper_ui.module',
        'basename' => 'feeds_tamper_ui.module',
        'name' => 'feeds_tamper_ui',
        'info' => 
        array (
          'name' => 'Feeds Tamper Admin UI',
          'description' => 'Administrative UI for Feeds Tamper module.',
          'package' => 'Feeds',
          'dependencies' => 
          array (
            0 => 'feeds_tamper',
            1 => 'feeds_ui',
          ),
          'files' => 
          array (
            0 => 'tests/feeds_tamper_ui.test',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta3+55-dev',
          'project' => 'feeds_tamper',
          'datestamp' => '1364000251',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'feeds_tamper',
        'version' => '7.x-1.0-beta3+55-dev',
      ),
      'feeds_tamper' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds_tamper/feeds_tamper.module',
        'basename' => 'feeds_tamper.module',
        'name' => 'feeds_tamper',
        'info' => 
        array (
          'name' => 'Feeds Tamper',
          'description' => 'Modify feeds data before it gets saved.',
          'package' => 'Feeds',
          'dependencies' => 
          array (
            0 => 'feeds',
          ),
          'files' => 
          array (
            0 => 'tests/feeds_tamper.test',
            1 => 'tests/feeds_tamper_plugins.test',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta3+55-dev',
          'project' => 'feeds_tamper',
          'datestamp' => '1364000251',
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'feeds_tamper',
        'version' => '7.x-1.0-beta3+55-dev',
      ),
      'backup_migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/backup_migrate/backup_migrate.module',
        'basename' => 'backup_migrate.module',
        'name' => 'backup_migrate',
        'info' => 
        array (
          'name' => 'Backup and Migrate',
          'description' => 'Backup the Drupal database and files or migrate them to another environment.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'backup_migrate.module',
            1 => 'backup_migrate.install',
            2 => 'includes/destinations.inc',
            3 => 'includes/profiles.inc',
            4 => 'includes/schedules.inc',
          ),
          'configure' => 'admin/config/system/backup_migrate',
          'version' => '7.x-3.x-dev',
          'project' => 'backup_migrate',
          'datestamp' => '1371515802',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7303',
        'project' => 'backup_migrate',
        'version' => '7.x-3.x-dev',
      ),
      'multifield' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/multifield/multifield.module',
        'basename' => 'multifield.module',
        'name' => 'multifield',
        'info' => 
        array (
          'name' => 'Multifield',
          'description' => 'Provides a combo field type.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'configure' => 'admin/structure/multifield',
          'version' => '7.x-1.0-unstable6+2-dev',
          'project' => 'multifield',
          'datestamp' => '1380592214',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'project' => 'multifield',
        'version' => '7.x-1.0-unstable6+2-dev',
      ),
      'geocoder' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/geocoder/geocoder.module',
        'basename' => 'geocoder.module',
        'name' => 'geocoder',
        'info' => 
        array (
          'name' => 'Geocoder',
          'description' => 'An API and widget to geocode various known data into other GIS data types.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geophp',
            1 => 'ctools',
          ),
          'version' => '7.x-1.2',
          'project' => 'geocoder',
          'datestamp' => '1346083034',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'geocoder',
        'version' => '7.x-1.2',
      ),
      'features' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/features/features.module',
        'basename' => 'features.module',
        'name' => 'features',
        'info' => 
        array (
          'name' => 'Features',
          'description' => 'Provides feature management for Drupal.',
          'core' => '7.x',
          'package' => 'Features',
          'files' => 
          array (
            0 => 'tests/features.test',
          ),
          'configure' => 'admin/structure/features/settings',
          'version' => '7.x-2.0-rc2',
          'project' => 'features',
          'datestamp' => '1375464067',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'project' => 'features',
        'version' => '7.x-2.0-rc2',
      ),
      'apc_admin' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/apc_admin/apc_admin.module',
        'basename' => 'apc_admin.module',
        'name' => 'apc_admin',
        'info' => 
        array (
          'name' => 'APC Admin',
          'description' => 'Manage APC from Drupal administration interface',
          'package' => 'Performance',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'apc_admin.module',
          ),
          'dependencies' => 
          array (
            0 => 'libraries',
          ),
          'configure' => 'admin/config/development/performance/apc_admin',
          'version' => '7.x-1.0',
          'project' => 'apc_admin',
          'datestamp' => '1361061816',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'apc_admin',
        'version' => '7.x-1.0',
      ),
      'node_export_features' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/node_export/modules/node_export_features/node_export_features.module',
        'basename' => 'node_export_features.module',
        'name' => 'node_export_features',
        'info' => 
        array (
          'name' => 'Node export features',
          'description' => 'Adds Features support to Node export, so you can put your exports into Features modules.',
          'dependencies' => 
          array (
            0 => 'node_export',
            1 => 'uuid',
            2 => 'features',
          ),
          'core' => '7.x',
          'package' => 'Node export',
          'version' => '7.x-3.0',
          'project' => 'node_export',
          'datestamp' => '1345435979',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'node_export',
        'version' => '7.x-3.0',
      ),
      'node_export_dependency' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/node_export/modules/node_export_dependency/node_export_dependency.module',
        'basename' => 'node_export_dependency.module',
        'name' => 'node_export_dependency',
        'info' => 
        array (
          'name' => 'Node export dependency (experimental)',
          'description' => 'Helps maintain relationships to dependent entities.  Intended to make Node export relation obsolete.',
          'dependencies' => 
          array (
            0 => 'node_export',
            1 => 'uuid',
          ),
          'core' => '7.x',
          'package' => 'Node export',
          'version' => '7.x-3.0',
          'project' => 'node_export',
          'datestamp' => '1345435979',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'node_export',
        'version' => '7.x-3.0',
      ),
      'node_export_relation' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/node_export/modules/node_export_relation/node_export_relation.module',
        'basename' => 'node_export_relation.module',
        'name' => 'node_export_relation',
        'info' => 
        array (
          'name' => 'Node export relation (deprecated)',
          'description' => 'Helps maintain relationships.  Supports node references, taxonomy, and organic groups.',
          'dependencies' => 
          array (
            0 => 'node_export',
            1 => 'uuid',
          ),
          'core' => '7.x',
          'package' => 'Node export',
          'version' => '7.x-3.0',
          'project' => 'node_export',
          'datestamp' => '1345435979',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'node_export',
        'version' => '7.x-3.0',
      ),
      'node_export_feeds' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/node_export/modules/node_export_feeds/node_export_feeds.module',
        'basename' => 'node_export_feeds.module',
        'name' => 'node_export_feeds',
        'info' => 
        array (
          'name' => 'Node export feeds',
          'description' => 'Node export feeds importer.  This is a more advanced importer than the one built into node export, but tricky to use.',
          'core' => '7.x',
          'package' => 'Node export',
          'dependencies' => 
          array (
            0 => 'feeds',
            1 => 'node_export',
          ),
          'files' => 
          array (
            0 => 'FeedsNodeExportParser.inc',
            1 => 'FeedsNodeExportProcessor.inc',
          ),
          'version' => '7.x-3.0',
          'project' => 'node_export',
          'datestamp' => '1345435979',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'node_export',
        'version' => '7.x-3.0',
      ),
      'node_export' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/node_export/node_export.module',
        'basename' => 'node_export.module',
        'name' => 'node_export',
        'info' => 
        array (
          'name' => 'Node export',
          'description' => 'Allows users to export content and then import into another Drupal installation.',
          'dependencies' => 
          array (
            0 => 'uuid',
          ),
          'core' => '7.x',
          'package' => 'Node export',
          'configure' => 'admin/config/content/node_export',
          'files' => 
          array (
            0 => 'views/views_handler_field_node_link_export.inc',
          ),
          'version' => '7.x-3.0',
          'project' => 'node_export',
          'datestamp' => '1345435979',
          'php' => '5.2.4',
        ),
        'schema_version' => '7303',
        'project' => 'node_export',
        'version' => '7.x-3.0',
      ),
      'select_or_other' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/select_or_other/select_or_other.module',
        'basename' => 'select_or_other.module',
        'name' => 'select_or_other',
        'info' => 
        array (
          'name' => 'Select (or other)',
          'description' => 'Provides a select box form element with additional option \'Other\' to give a textfield.',
          'core' => '7.x',
          'package' => 'Fields',
          'version' => '7.x-2.19',
          'project' => 'select_or_other',
          'datestamp' => '1375892951',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'project' => 'select_or_other',
        'version' => '7.x-2.19',
      ),
      'inline_entity_form' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/inline_entity_form/inline_entity_form.module',
        'basename' => 'inline_entity_form.module',
        'name' => 'inline_entity_form',
        'info' => 
        array (
          'name' => 'Inline Entity Form',
          'description' => 'Provides a widget for inline management (creation, modification, removal) of referenced entities. ',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'system (>7.14)',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/entity.inline_entity_form.inc',
            1 => 'includes/node.inline_entity_form.inc',
            2 => 'includes/taxonomy_term.inline_entity_form.inc',
            3 => 'includes/commerce_product.inline_entity_form.inc',
            4 => 'includes/commerce_line_item.inline_entity_form.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'inline_entity_form',
          'datestamp' => '1368798850',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'inline_entity_form',
        'version' => '7.x-1.2',
      ),
      'ckeditor' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/ckeditor/ckeditor.module',
        'basename' => 'ckeditor.module',
        'name' => 'ckeditor',
        'info' => 
        array (
          'name' => 'CKEditor',
          'description' => 'Enables CKEditor (WYSIWYG HTML editor) for use instead of plain text fields.',
          'core' => '7.x',
          'package' => 'User interface',
          'configure' => 'admin/config/content/ckeditor',
          'version' => '7.x-1.13',
          'project' => 'ckeditor',
          'datestamp' => '1365759619',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'project' => 'ckeditor',
        'version' => '7.x-1.13',
      ),
      'content_access_rules' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/content_access/content_access_rules/content_access_rules.module',
        'basename' => 'content_access_rules.module',
        'name' => 'content_access_rules',
        'info' => 
        array (
          'name' => 'Content Access Rules Integrations',
          'description' => 'Integrates Rules with Content access. Allows to act on access events, conditions, and actions.',
          'package' => 'Access control',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'content_access',
            1 => 'rules',
          ),
          'files' => 
          array (
            0 => 'content_access.rules.inc',
          ),
          'version' => '7.x-1.2-beta2',
          'project' => 'content_access',
          'datestamp' => '1366014321',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'content_access',
        'version' => '7.x-1.2-beta2',
      ),
      'content_access' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/content_access/content_access.module',
        'basename' => 'content_access.module',
        'name' => 'content_access',
        'info' => 
        array (
          'name' => 'Content Access',
          'description' => 'Provides flexible content access control.',
          'core' => '7.x',
          'package' => 'Access control',
          'files' => 
          array (
            0 => 'content_access.rules.inc',
            1 => 'tests/content_access.test',
            2 => 'tests/content_access_acl.test',
          ),
          'version' => '7.x-1.2-beta2',
          'project' => 'content_access',
          'datestamp' => '1366014321',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'project' => 'content_access',
        'version' => '7.x-1.2-beta2',
      ),
      'cmf' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/cmf/cmf.module',
        'basename' => 'cmf.module',
        'name' => 'cmf',
        'info' => 
        array (
          'name' => 'Content management filter',
          'description' => 'This module adds an easier way for administrators to filter the content on a Drupal site for administration purposes.',
          'package' => 'Administration',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'cmf',
          'datestamp' => '1380559131',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'cmf',
        'version' => '7.x-1.x-dev',
      ),
      'pathauto' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/pathauto/pathauto.module',
        'basename' => 'pathauto.module',
        'name' => 'pathauto',
        'info' => 
        array (
          'name' => 'Pathauto',
          'description' => 'Provides a mechanism for modules to automatically generate aliases for the content they manage.',
          'dependencies' => 
          array (
            0 => 'path',
            1 => 'token',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'pathauto.test',
          ),
          'configure' => 'admin/config/search/path/patterns',
          'recommends' => 
          array (
            0 => 'redirect',
          ),
          'version' => '7.x-1.2',
          'project' => 'pathauto',
          'datestamp' => '1344525185',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'project' => 'pathauto',
        'version' => '7.x-1.2',
      ),
      'eva' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/eva/eva.module',
        'basename' => 'eva.module',
        'name' => 'eva',
        'info' => 
        array (
          'name' => 'Eva',
          'description' => 'Provides a Views display type that can be attached to entities.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'package' => 'Views',
          'files' => 
          array (
            0 => 'eva_plugin_display_entity.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'eva',
          'datestamp' => '1343701935',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'eva',
        'version' => '7.x-1.2',
      ),
      'better_exposed_filters' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/better_exposed_filters/better_exposed_filters.module',
        'basename' => 'better_exposed_filters.module',
        'name' => 'better_exposed_filters',
        'info' => 
        array (
          'name' => 'Better Exposed Filters',
          'description' => 'Allow the use of checkboxes or radio buttons for exposed Views filters',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'better_exposed_filters.module',
            1 => 'better_exposed_filters.views.inc',
            2 => 'better_exposed_filters_exposed_form_plugin.inc',
            3 => 'better_exposed_filters.theme',
          ),
          'version' => '7.x-3.0-beta3',
          'project' => 'better_exposed_filters',
          'datestamp' => '1349281866',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'better_exposed_filters',
        'version' => '7.x-3.0-beta3',
      ),
      'link' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/link/link.module',
        'basename' => 'link.module',
        'name' => 'link',
        'info' => 
        array (
          'name' => 'Link',
          'description' => 'Defines simple link field types.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'link.module',
            1 => 'link.migrate.inc',
            2 => 'tests/link.test',
            3 => 'tests/link.attribute.test',
            4 => 'tests/link.crud.test',
            5 => 'tests/link.crud_browser.test',
            6 => 'tests/link.token.test',
            7 => 'tests/link.validate.test',
            8 => 'views/link_views_handler_argument_target.inc',
            9 => 'views/link_views_handler_filter_protocol.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'link',
          'datestamp' => '1360444361',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'link',
        'version' => '7.x-1.1',
      ),
      'form_builder_webform' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/form_builder/modules/webform/form_builder_webform.module',
        'basename' => 'form_builder_webform.module',
        'name' => 'form_builder_webform',
        'info' => 
        array (
          'name' => 'Form builder Webform UI',
          'description' => 'Form builder integration for the Webform module.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'form_builder',
            1 => 'webform',
          ),
          'version' => '7.x-1.4',
          'project' => 'form_builder',
          'datestamp' => '1371628554',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'form_builder',
        'version' => '7.x-1.4',
      ),
      'form_builder_examples' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/form_builder/examples/form_builder_examples.module',
        'basename' => 'form_builder_examples.module',
        'name' => 'form_builder_examples',
        'info' => 
        array (
          'name' => 'Form builder examples',
          'description' => 'Form builder support for CCK, Webform, and Profile modules.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'form_builder',
          ),
          'version' => '7.x-1.4',
          'project' => 'form_builder',
          'datestamp' => '1371628554',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'form_builder',
        'version' => '7.x-1.4',
      ),
      'form_builder' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/form_builder/form_builder.module',
        'basename' => 'form_builder.module',
        'name' => 'form_builder',
        'info' => 
        array (
          'name' => 'Form builder',
          'description' => 'Form building framework.',
          'dependencies' => 
          array (
            0 => 'options_element',
          ),
          'core' => '7.x',
          'version' => '7.x-1.4',
          'project' => 'form_builder',
          'datestamp' => '1371628554',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'form_builder',
        'version' => '7.x-1.4',
      ),
      'admin_menu_toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/admin_menu/admin_menu_toolbar/admin_menu_toolbar.module',
        'basename' => 'admin_menu_toolbar.module',
        'name' => 'admin_menu_toolbar',
        'info' => 
        array (
          'name' => 'Administration menu Toolbar style',
          'description' => 'A better Toolbar.',
          'package' => 'Administration',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'admin_menu',
          ),
          'version' => '7.x-3.0-rc4',
          'project' => 'admin_menu',
          'datestamp' => '1359651687',
          'php' => '5.2.4',
        ),
        'schema_version' => '6300',
        'project' => 'admin_menu',
        'version' => '7.x-3.0-rc4',
      ),
      'admin_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/admin_menu/admin_devel/admin_devel.module',
        'basename' => 'admin_devel.module',
        'name' => 'admin_devel',
        'info' => 
        array (
          'name' => 'Administration Development tools',
          'description' => 'Administration and debugging functionality for developers and site builders.',
          'package' => 'Administration',
          'core' => '7.x',
          'scripts' => 
          array (
            0 => 'admin_devel.js',
          ),
          'version' => '7.x-3.0-rc4',
          'project' => 'admin_menu',
          'datestamp' => '1359651687',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'admin_menu',
        'version' => '7.x-3.0-rc4',
      ),
      'admin_menu' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/admin_menu/admin_menu.module',
        'basename' => 'admin_menu.module',
        'name' => 'admin_menu',
        'info' => 
        array (
          'name' => 'Administration menu',
          'description' => 'Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).',
          'package' => 'Administration',
          'core' => '7.x',
          'configure' => 'admin/config/administration/admin_menu',
          'dependencies' => 
          array (
            0 => 'system (>7.10)',
          ),
          'files' => 
          array (
            0 => 'tests/admin_menu.test',
          ),
          'version' => '7.x-3.0-rc4',
          'project' => 'admin_menu',
          'datestamp' => '1359651687',
          'php' => '5.2.4',
        ),
        'schema_version' => '7304',
        'project' => 'admin_menu',
        'version' => '7.x-3.0-rc4',
      ),
      'custom_formatters' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/custom_formatters/custom_formatters.module',
        'basename' => 'custom_formatters.module',
        'name' => 'custom_formatters',
        'info' => 
        array (
          'name' => 'Custom Formatters',
          'description' => 'Allows users to easily define custom CCK Formatters.',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'field',
          ),
          'package' => 'Fields',
          'configure' => 'admin/structure/formatters',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'engines/php.inc',
            1 => 'engines/token.inc',
            2 => 'includes/contextual.inc',
            3 => 'includes/ctools.inc',
            4 => 'includes/custom_formatters.admin.inc',
            5 => 'includes/custom_formatters.inc',
            6 => 'includes/devel_generate.inc',
            7 => 'includes/features.inc',
            8 => 'includes/field.inc',
            9 => 'includes/form_builder.inc',
            10 => 'includes/help.inc',
            11 => 'includes/insert.inc',
            12 => 'includes/libraries.inc',
            13 => 'includes/node.inc',
            14 => 'includes/system.inc',
            15 => 'includes/taxonomy.inc',
            16 => 'includes/token.inc',
            17 => 'includes/user.inc',
            18 => 'plugins/export_ui/custom_formatters.inc',
            19 => 'plugins/export_ui/custom_formatters_ui.class.php',
            20 => 'custom_formatters.module',
          ),
          'version' => '7.x-2.2',
          'project' => 'custom_formatters',
          'datestamp' => '1341407783',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'project' => 'custom_formatters',
        'version' => '7.x-2.2',
      ),
      'entity_token' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entity/entity_token.module',
        'basename' => 'entity_token.module',
        'name' => 'entity_token',
        'info' => 
        array (
          'name' => 'Entity tokens',
          'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity_token.tokens.inc',
            1 => 'entity_token.module',
          ),
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.2',
          'project' => 'entity',
          'datestamp' => '1376493705',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entity',
        'version' => '7.x-1.2',
      ),
      'entity' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/entity/entity.module',
        'basename' => 'entity.module',
        'name' => 'entity',
        'info' => 
        array (
          'name' => 'Entity API',
          'description' => 'Enables modules to work with any entity type and to provide entities.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity.features.inc',
            1 => 'entity.i18n.inc',
            2 => 'entity.info.inc',
            3 => 'entity.rules.inc',
            4 => 'entity.test',
            5 => 'includes/entity.inc',
            6 => 'includes/entity.controller.inc',
            7 => 'includes/entity.ui.inc',
            8 => 'includes/entity.wrapper.inc',
            9 => 'views/entity.views.inc',
            10 => 'views/handlers/entity_views_field_handler_helper.inc',
            11 => 'views/handlers/entity_views_handler_area_entity.inc',
            12 => 'views/handlers/entity_views_handler_field_boolean.inc',
            13 => 'views/handlers/entity_views_handler_field_date.inc',
            14 => 'views/handlers/entity_views_handler_field_duration.inc',
            15 => 'views/handlers/entity_views_handler_field_entity.inc',
            16 => 'views/handlers/entity_views_handler_field_field.inc',
            17 => 'views/handlers/entity_views_handler_field_numeric.inc',
            18 => 'views/handlers/entity_views_handler_field_options.inc',
            19 => 'views/handlers/entity_views_handler_field_text.inc',
            20 => 'views/handlers/entity_views_handler_field_uri.inc',
            21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
            22 => 'views/handlers/entity_views_handler_relationship.inc',
            23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'entity',
          'datestamp' => '1376493705',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'entity',
        'version' => '7.x-1.2',
      ),
      'views_export' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/views/views_export/views_export.module',
        'basename' => 'views_export.module',
        'name' => 'views_export',
        'info' => 
        array (
          'dependencies' => 
          array (
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'views' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/views/views.module',
        'basename' => 'views.module',
        'name' => 'views',
        'info' => 
        array (
          'name' => 'Views',
          'description' => 'Create customized lists and queries from your database.',
          'package' => 'Views',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/views.css',
            ),
          ),
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'handlers/views_handler_area.inc',
            1 => 'handlers/views_handler_area_result.inc',
            2 => 'handlers/views_handler_area_text.inc',
            3 => 'handlers/views_handler_area_text_custom.inc',
            4 => 'handlers/views_handler_area_view.inc',
            5 => 'handlers/views_handler_argument.inc',
            6 => 'handlers/views_handler_argument_date.inc',
            7 => 'handlers/views_handler_argument_formula.inc',
            8 => 'handlers/views_handler_argument_many_to_one.inc',
            9 => 'handlers/views_handler_argument_null.inc',
            10 => 'handlers/views_handler_argument_numeric.inc',
            11 => 'handlers/views_handler_argument_string.inc',
            12 => 'handlers/views_handler_argument_group_by_numeric.inc',
            13 => 'handlers/views_handler_field.inc',
            14 => 'handlers/views_handler_field_counter.inc',
            15 => 'handlers/views_handler_field_boolean.inc',
            16 => 'handlers/views_handler_field_contextual_links.inc',
            17 => 'handlers/views_handler_field_custom.inc',
            18 => 'handlers/views_handler_field_date.inc',
            19 => 'handlers/views_handler_field_entity.inc',
            20 => 'handlers/views_handler_field_markup.inc',
            21 => 'handlers/views_handler_field_math.inc',
            22 => 'handlers/views_handler_field_numeric.inc',
            23 => 'handlers/views_handler_field_prerender_list.inc',
            24 => 'handlers/views_handler_field_time_interval.inc',
            25 => 'handlers/views_handler_field_serialized.inc',
            26 => 'handlers/views_handler_field_machine_name.inc',
            27 => 'handlers/views_handler_field_url.inc',
            28 => 'handlers/views_handler_filter.inc',
            29 => 'handlers/views_handler_filter_boolean_operator.inc',
            30 => 'handlers/views_handler_filter_boolean_operator_string.inc',
            31 => 'handlers/views_handler_filter_combine.inc',
            32 => 'handlers/views_handler_filter_date.inc',
            33 => 'handlers/views_handler_filter_equality.inc',
            34 => 'handlers/views_handler_filter_entity_bundle.inc',
            35 => 'handlers/views_handler_filter_group_by_numeric.inc',
            36 => 'handlers/views_handler_filter_in_operator.inc',
            37 => 'handlers/views_handler_filter_many_to_one.inc',
            38 => 'handlers/views_handler_filter_numeric.inc',
            39 => 'handlers/views_handler_filter_string.inc',
            40 => 'handlers/views_handler_relationship.inc',
            41 => 'handlers/views_handler_relationship_groupwise_max.inc',
            42 => 'handlers/views_handler_sort.inc',
            43 => 'handlers/views_handler_sort_date.inc',
            44 => 'handlers/views_handler_sort_formula.inc',
            45 => 'handlers/views_handler_sort_group_by_numeric.inc',
            46 => 'handlers/views_handler_sort_menu_hierarchy.inc',
            47 => 'handlers/views_handler_sort_random.inc',
            48 => 'includes/base.inc',
            49 => 'includes/handlers.inc',
            50 => 'includes/plugins.inc',
            51 => 'includes/view.inc',
            52 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
            53 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
            54 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
            55 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
            56 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
            57 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
            58 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
            59 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
            60 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
            61 => 'modules/book/views_plugin_argument_default_book_root.inc',
            62 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
            63 => 'modules/comment/views_handler_field_comment.inc',
            64 => 'modules/comment/views_handler_field_comment_depth.inc',
            65 => 'modules/comment/views_handler_field_comment_link.inc',
            66 => 'modules/comment/views_handler_field_comment_link_approve.inc',
            67 => 'modules/comment/views_handler_field_comment_link_delete.inc',
            68 => 'modules/comment/views_handler_field_comment_link_edit.inc',
            69 => 'modules/comment/views_handler_field_comment_link_reply.inc',
            70 => 'modules/comment/views_handler_field_comment_node_link.inc',
            71 => 'modules/comment/views_handler_field_comment_username.inc',
            72 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
            73 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
            74 => 'modules/comment/views_handler_field_node_comment.inc',
            75 => 'modules/comment/views_handler_field_node_new_comments.inc',
            76 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
            77 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
            78 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
            79 => 'modules/comment/views_handler_filter_node_comment.inc',
            80 => 'modules/comment/views_handler_sort_comment_thread.inc',
            81 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
            82 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
            83 => 'modules/comment/views_plugin_row_comment_rss.inc',
            84 => 'modules/comment/views_plugin_row_comment_view.inc',
            85 => 'modules/contact/views_handler_field_contact_link.inc',
            86 => 'modules/field/views_handler_field_field.inc',
            87 => 'modules/field/views_handler_relationship_entity_reverse.inc',
            88 => 'modules/field/views_handler_argument_field_list.inc',
            89 => 'modules/field/views_handler_argument_field_list_string.inc',
            90 => 'modules/field/views_handler_filter_field_list.inc',
            91 => 'modules/filter/views_handler_field_filter_format_name.inc',
            92 => 'modules/locale/views_handler_field_node_language.inc',
            93 => 'modules/locale/views_handler_filter_node_language.inc',
            94 => 'modules/locale/views_handler_argument_locale_group.inc',
            95 => 'modules/locale/views_handler_argument_locale_language.inc',
            96 => 'modules/locale/views_handler_field_locale_group.inc',
            97 => 'modules/locale/views_handler_field_locale_language.inc',
            98 => 'modules/locale/views_handler_field_locale_link_edit.inc',
            99 => 'modules/locale/views_handler_filter_locale_group.inc',
            100 => 'modules/locale/views_handler_filter_locale_language.inc',
            101 => 'modules/locale/views_handler_filter_locale_version.inc',
            102 => 'modules/node/views_handler_argument_dates_various.inc',
            103 => 'modules/node/views_handler_argument_node_language.inc',
            104 => 'modules/node/views_handler_argument_node_nid.inc',
            105 => 'modules/node/views_handler_argument_node_type.inc',
            106 => 'modules/node/views_handler_argument_node_vid.inc',
            107 => 'modules/node/views_handler_argument_node_uid_revision.inc',
            108 => 'modules/node/views_handler_field_history_user_timestamp.inc',
            109 => 'modules/node/views_handler_field_node.inc',
            110 => 'modules/node/views_handler_field_node_link.inc',
            111 => 'modules/node/views_handler_field_node_link_delete.inc',
            112 => 'modules/node/views_handler_field_node_link_edit.inc',
            113 => 'modules/node/views_handler_field_node_revision.inc',
            114 => 'modules/node/views_handler_field_node_revision_link.inc',
            115 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
            116 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
            117 => 'modules/node/views_handler_field_node_path.inc',
            118 => 'modules/node/views_handler_field_node_type.inc',
            119 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
            120 => 'modules/node/views_handler_filter_node_access.inc',
            121 => 'modules/node/views_handler_filter_node_status.inc',
            122 => 'modules/node/views_handler_filter_node_type.inc',
            123 => 'modules/node/views_handler_filter_node_uid_revision.inc',
            124 => 'modules/node/views_plugin_argument_default_node.inc',
            125 => 'modules/node/views_plugin_argument_validate_node.inc',
            126 => 'modules/node/views_plugin_row_node_rss.inc',
            127 => 'modules/node/views_plugin_row_node_view.inc',
            128 => 'modules/profile/views_handler_field_profile_date.inc',
            129 => 'modules/profile/views_handler_field_profile_list.inc',
            130 => 'modules/profile/views_handler_filter_profile_selection.inc',
            131 => 'modules/search/views_handler_argument_search.inc',
            132 => 'modules/search/views_handler_field_search_score.inc',
            133 => 'modules/search/views_handler_filter_search.inc',
            134 => 'modules/search/views_handler_sort_search_score.inc',
            135 => 'modules/search/views_plugin_row_search_view.inc',
            136 => 'modules/statistics/views_handler_field_accesslog_path.inc',
            137 => 'modules/system/views_handler_argument_file_fid.inc',
            138 => 'modules/system/views_handler_field_file.inc',
            139 => 'modules/system/views_handler_field_file_extension.inc',
            140 => 'modules/system/views_handler_field_file_filemime.inc',
            141 => 'modules/system/views_handler_field_file_uri.inc',
            142 => 'modules/system/views_handler_field_file_status.inc',
            143 => 'modules/system/views_handler_filter_file_status.inc',
            144 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
            145 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
            146 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
            147 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
            148 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
            149 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
            150 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
            151 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
            152 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
            153 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
            154 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
            155 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
            156 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
            157 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
            158 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
            159 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
            160 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
            161 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
            162 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
            163 => 'modules/system/views_handler_filter_system_type.inc',
            164 => 'modules/translation/views_handler_argument_node_tnid.inc',
            165 => 'modules/translation/views_handler_field_node_link_translate.inc',
            166 => 'modules/translation/views_handler_field_node_translation_link.inc',
            167 => 'modules/translation/views_handler_filter_node_tnid.inc',
            168 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
            169 => 'modules/translation/views_handler_relationship_translation.inc',
            170 => 'modules/user/views_handler_argument_user_uid.inc',
            171 => 'modules/user/views_handler_argument_users_roles_rid.inc',
            172 => 'modules/user/views_handler_field_user.inc',
            173 => 'modules/user/views_handler_field_user_language.inc',
            174 => 'modules/user/views_handler_field_user_link.inc',
            175 => 'modules/user/views_handler_field_user_link_cancel.inc',
            176 => 'modules/user/views_handler_field_user_link_edit.inc',
            177 => 'modules/user/views_handler_field_user_mail.inc',
            178 => 'modules/user/views_handler_field_user_name.inc',
            179 => 'modules/user/views_handler_field_user_permissions.inc',
            180 => 'modules/user/views_handler_field_user_picture.inc',
            181 => 'modules/user/views_handler_field_user_roles.inc',
            182 => 'modules/user/views_handler_filter_user_current.inc',
            183 => 'modules/user/views_handler_filter_user_name.inc',
            184 => 'modules/user/views_handler_filter_user_permissions.inc',
            185 => 'modules/user/views_handler_filter_user_roles.inc',
            186 => 'modules/user/views_plugin_argument_default_current_user.inc',
            187 => 'modules/user/views_plugin_argument_default_user.inc',
            188 => 'modules/user/views_plugin_argument_validate_user.inc',
            189 => 'modules/user/views_plugin_row_user_view.inc',
            190 => 'plugins/views_plugin_access.inc',
            191 => 'plugins/views_plugin_access_none.inc',
            192 => 'plugins/views_plugin_access_perm.inc',
            193 => 'plugins/views_plugin_access_role.inc',
            194 => 'plugins/views_plugin_argument_default.inc',
            195 => 'plugins/views_plugin_argument_default_php.inc',
            196 => 'plugins/views_plugin_argument_default_fixed.inc',
            197 => 'plugins/views_plugin_argument_default_raw.inc',
            198 => 'plugins/views_plugin_argument_validate.inc',
            199 => 'plugins/views_plugin_argument_validate_numeric.inc',
            200 => 'plugins/views_plugin_argument_validate_php.inc',
            201 => 'plugins/views_plugin_cache.inc',
            202 => 'plugins/views_plugin_cache_none.inc',
            203 => 'plugins/views_plugin_cache_time.inc',
            204 => 'plugins/views_plugin_display.inc',
            205 => 'plugins/views_plugin_display_attachment.inc',
            206 => 'plugins/views_plugin_display_block.inc',
            207 => 'plugins/views_plugin_display_default.inc',
            208 => 'plugins/views_plugin_display_embed.inc',
            209 => 'plugins/views_plugin_display_extender.inc',
            210 => 'plugins/views_plugin_display_feed.inc',
            211 => 'plugins/views_plugin_display_page.inc',
            212 => 'plugins/views_plugin_exposed_form_basic.inc',
            213 => 'plugins/views_plugin_exposed_form.inc',
            214 => 'plugins/views_plugin_exposed_form_input_required.inc',
            215 => 'plugins/views_plugin_localization_core.inc',
            216 => 'plugins/views_plugin_localization.inc',
            217 => 'plugins/views_plugin_localization_none.inc',
            218 => 'plugins/views_plugin_pager.inc',
            219 => 'plugins/views_plugin_pager_full.inc',
            220 => 'plugins/views_plugin_pager_mini.inc',
            221 => 'plugins/views_plugin_pager_none.inc',
            222 => 'plugins/views_plugin_pager_some.inc',
            223 => 'plugins/views_plugin_query.inc',
            224 => 'plugins/views_plugin_query_default.inc',
            225 => 'plugins/views_plugin_row.inc',
            226 => 'plugins/views_plugin_row_fields.inc',
            227 => 'plugins/views_plugin_row_rss_fields.inc',
            228 => 'plugins/views_plugin_style.inc',
            229 => 'plugins/views_plugin_style_default.inc',
            230 => 'plugins/views_plugin_style_grid.inc',
            231 => 'plugins/views_plugin_style_list.inc',
            232 => 'plugins/views_plugin_style_jump_menu.inc',
            233 => 'plugins/views_plugin_style_mapping.inc',
            234 => 'plugins/views_plugin_style_rss.inc',
            235 => 'plugins/views_plugin_style_summary.inc',
            236 => 'plugins/views_plugin_style_summary_jump_menu.inc',
            237 => 'plugins/views_plugin_style_summary_unformatted.inc',
            238 => 'plugins/views_plugin_style_table.inc',
            239 => 'tests/handlers/views_handler_area_text.test',
            240 => 'tests/handlers/views_handler_argument_null.test',
            241 => 'tests/handlers/views_handler_argument_string.test',
            242 => 'tests/handlers/views_handler_field.test',
            243 => 'tests/handlers/views_handler_field_boolean.test',
            244 => 'tests/handlers/views_handler_field_custom.test',
            245 => 'tests/handlers/views_handler_field_counter.test',
            246 => 'tests/handlers/views_handler_field_date.test',
            247 => 'tests/handlers/views_handler_field_file_size.test',
            248 => 'tests/handlers/views_handler_field_math.test',
            249 => 'tests/handlers/views_handler_field_url.test',
            250 => 'tests/handlers/views_handler_field_xss.test',
            251 => 'tests/handlers/views_handler_filter_combine.test',
            252 => 'tests/handlers/views_handler_filter_date.test',
            253 => 'tests/handlers/views_handler_filter_equality.test',
            254 => 'tests/handlers/views_handler_filter_in_operator.test',
            255 => 'tests/handlers/views_handler_filter_numeric.test',
            256 => 'tests/handlers/views_handler_filter_string.test',
            257 => 'tests/handlers/views_handler_sort_random.test',
            258 => 'tests/handlers/views_handler_sort_date.test',
            259 => 'tests/handlers/views_handler_sort.test',
            260 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
            261 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
            262 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
            263 => 'tests/plugins/views_plugin_display.test',
            264 => 'tests/styles/views_plugin_style_jump_menu.test',
            265 => 'tests/styles/views_plugin_style.test',
            266 => 'tests/styles/views_plugin_style_base.test',
            267 => 'tests/styles/views_plugin_style_mapping.test',
            268 => 'tests/styles/views_plugin_style_unformatted.test',
            269 => 'tests/views_access.test',
            270 => 'tests/views_analyze.test',
            271 => 'tests/views_basic.test',
            272 => 'tests/views_argument_default.test',
            273 => 'tests/views_argument_validator.test',
            274 => 'tests/views_exposed_form.test',
            275 => 'tests/field/views_fieldapi.test',
            276 => 'tests/views_glossary.test',
            277 => 'tests/views_groupby.test',
            278 => 'tests/views_handlers.test',
            279 => 'tests/views_module.test',
            280 => 'tests/views_pager.test',
            281 => 'tests/views_plugin_localization_test.inc',
            282 => 'tests/views_translatable.test',
            283 => 'tests/views_query.test',
            284 => 'tests/views_upgrade.test',
            285 => 'tests/views_test.views_default.inc',
            286 => 'tests/comment/views_handler_argument_comment_user_uid.test',
            287 => 'tests/comment/views_handler_filter_comment_user_uid.test',
            288 => 'tests/node/views_node_revision_relations.test',
            289 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
            290 => 'tests/user/views_handler_field_user_name.test',
            291 => 'tests/user/views_user_argument_default.test',
            292 => 'tests/user/views_user_argument_validate.test',
            293 => 'tests/user/views_user.test',
            294 => 'tests/views_cache.test',
            295 => 'tests/views_view.test',
            296 => 'tests/views_ui.test',
          ),
          'version' => '7.x-3.7',
          'project' => 'views',
          'datestamp' => '1365499236',
        ),
        'schema_version' => '7301',
        'project' => 'views',
        'version' => '7.x-3.7',
      ),
      'views_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/views/views_ui.module',
        'basename' => 'views_ui.module',
        'name' => 'views_ui',
        'info' => 
        array (
          'name' => 'Views UI',
          'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
          'package' => 'Views',
          'core' => '7.x',
          'configure' => 'admin/structure/views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_ui.module',
            1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
          ),
          'version' => '7.x-3.7',
          'project' => 'views',
          'datestamp' => '1365499236',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'views',
        'version' => '7.x-3.7',
      ),
      'lightbox2' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/lightbox2/lightbox2.module',
        'basename' => 'lightbox2.module',
        'name' => 'lightbox2',
        'info' => 
        array (
          'name' => 'Lightbox2',
          'description' => 'Enables Lightbox2 for Drupal',
          'core' => '7.x',
          'package' => 'User interface',
          'files' => 
          array (
            0 => 'lightbox2.install',
            1 => 'lightbox2.module',
            2 => 'lightbox2.formatter.inc',
            3 => 'lightbox2.admin.inc',
          ),
          'configure' => 'admin/config/user-interface/lightbox2',
          'version' => '7.x-1.0-beta1',
          'project' => 'lightbox2',
          'datestamp' => '1318819001',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6003',
        'project' => 'lightbox2',
        'version' => '7.x-1.0-beta1',
      ),
      'feeds_tamper_string2id' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/contrib/feeds_tamper_string2id/feeds_tamper_string2id.module',
        'basename' => 'feeds_tamper_string2id.module',
        'name' => 'feeds_tamper_string2id',
        'info' => 
        array (
          'name' => 'Feeds Tamper: String to ID',
          'description' => 'feeds_tamper_string2id resolves strings pulled in from feeds importers and maps them to Drupal entity IDs, so they can be used as entity references.',
          'package' => 'Feeds',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'feeds',
          ),
          'version' => '7.x-1.0',
          'project' => 'feeds_tamper_string2id',
          'datestamp' => '1368618913',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'feeds_tamper_string2id',
        'version' => '7.x-1.0',
      ),
      'gdp_glue' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_glue/gdp_glue.module',
        'basename' => 'gdp_glue.module',
        'name' => 'gdp_glue',
        'info' => 
        array (
          'name' => 'GDP Glue',
          'description' => 'Glue code for GDP.',
          'package' => 'GDP Custom Code',
          'core' => '7.x',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_fields/gdp_fields.module',
        'basename' => 'gdp_fields.module',
        'name' => 'gdp_fields',
        'info' => 
        array (
          'name' => 'GDP Fields',
          'description' => 'TODO: Description of module',
          'package' => 'GDP Custom Code',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'gdp_fields.module',
            1 => 'gdp_fields.fields.inc',
            2 => 'gdp_fields.install',
            3 => 'gdp_fields/views/gdp_fields.views.inc',
          ),
          'dependencies' => 
          array (
            0 => 'list',
            1 => 'select_or_other',
            2 => 'views',
            3 => 'argfilters',
            4 => 'date',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7021',
        'project' => '',
        'version' => NULL,
      ),
      'gdp_scorecard' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_scorecard/gdp_scorecard.module',
        'basename' => 'gdp_scorecard.module',
        'name' => 'gdp_scorecard',
        'info' => 
        array (
          'name' => 'GDP Scorecard',
          'description' => 'TODO: Description of module',
          'package' => 'GDP Custom Code',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'gdp_scorecard.module',
            1 => 'js/gdp_scorecard.js',
          ),
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_create_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_create_fields/gdp_create_fields.module',
        'basename' => 'gdp_create_fields.module',
        'name' => 'gdp_create_fields',
        'info' => 
        array (
          'name' => 'Create Fields',
          'description' => 'Glue code for GDP.',
          'package' => 'Other',
          'core' => '7.x',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_csv_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_csv_generate/gdp_csv_generate.module',
        'basename' => 'gdp_csv_generate.module',
        'name' => 'gdp_csv_generate',
        'info' => 
        array (
          'name' => 'GDP CSV GENERATE',
          'description' => 'TODO: Description of module',
          'package' => 'GDP Custom Code',
          'core' => '7.x',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_efq' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_efq/gdp_efq.module',
        'basename' => 'gdp_efq.module',
        'name' => 'gdp_efq',
        'info' => 
        array (
          'name' => 'GDP EFQ',
          'description' => 'EFQ queries for GDP project.',
          'package' => 'GDP Custom Code',
          'core' => '7.x',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_chart' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_chart/gdp_chart.module',
        'basename' => 'gdp_chart.module',
        'name' => 'gdp_chart',
        'info' => 
        array (
          'name' => 'GDP CHART PAGE',
          'description' => 'TODO: Description of module',
          'package' => 'GDP Custom Code',
          'dependencies' => 
          array (
            0 => 'charts',
            1 => 'charts_google',
          ),
          'core' => '7.x',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_import' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_import/gdp_import.module',
        'basename' => 'gdp_import.module',
        'name' => 'gdp_import',
        'info' => 
        array (
          'name' => 'GDP CUSTOM FEED IMPORT',
          'description' => 'TODO: import multiple custom fields by feed module',
          'package' => 'GDP Custom Code',
          'core' => '7.x',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'gdp_form_labels' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/gdp_form_labels/gdp_form_labels.module',
        'basename' => 'gdp_form_labels.module',
        'name' => 'gdp_form_labels',
        'info' => 
        array (
          'name' => 'GDP Form Labels',
          'description' => 'This module allows to change the form fields labels with the field formatter value',
          'core' => '7.x',
          'version' => '7.x',
          'files' => 
          array (
            0 => 'gdp_form_labels.module',
          ),
          'project' => 'gdp_form_labels',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'gdp_form_labels',
        'version' => '7.x',
      ),
      'source_content_type_module' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/modules/custom/source_content_type_module/source_content_type_module.module',
        'basename' => 'source_content_type_module.module',
        'name' => 'source_content_type_module',
        'info' => 
        array (
          'name' => 'Source Content Type Module',
          'core' => '7.x',
          'package' => 'Features',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'strongarm',
            2 => 'text',
            3 => 'views',
          ),
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'strongarm:strongarm:1',
              1 => 'views:views_default:3.0',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'body',
            ),
            'field_instance' => 
            array (
              0 => 'node-source-body',
            ),
            'node' => 
            array (
              0 => 'source',
            ),
            'variable' => 
            array (
              0 => 'comment_anonymous_source',
              1 => 'comment_default_mode_source',
              2 => 'comment_default_per_page_source',
              3 => 'comment_form_location_source',
              4 => 'comment_preview_source',
              5 => 'comment_source',
              6 => 'comment_subject_field_source',
              7 => 'menu_options_source',
              8 => 'menu_parent_source',
              9 => 'node_options_source',
              10 => 'node_preview_source',
              11 => 'node_submitted_source',
            ),
            'views_view' => 
            array (
              0 => 'source_listing',
            ),
          ),
          'features_exclude' => 
          array (
            'dependencies' => 
            array (
              'ctools' => 'ctools',
            ),
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
    ),
    'themes' => 
    array (
      'boson' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.stage/sites/all/themes/boson/boson.info',
        'basename' => 'boson.info',
        'name' => 'Boson',
        'info' => 
        array (
          'name' => 'Boson',
          'description' => 'Clean and modern responsive Drupal theme.',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'screenshot' => 'screenshot.png',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/responsive.css',
              1 => 'css/prettyPhoto.css',
              2 => 'css/settings.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'js/jquery.prettyPhoto.js',
            1 => 'js/main.js',
          ),
          'regions' => 
          array (
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'highlighted' => 'Highlighted',
            'top_left' => 'Top Left',
            'top_right' => 'Top Right',
            'main_menu' => 'Main Menu',
            'search_box' => 'Search Box',
            'slider' => 'Slider',
            'featured' => 'Featured',
            'top_content' => 'Top Content',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'bottom_content' => 'Bottom Content',
            'postscript_first' => 'Postscript First',
            'postscript_second' => 'Postscript Second',
            'postscript_third' => 'Postscript Third',
            'postscript_fourth' => 'Postscript Fourth',
            'bottom' => 'Bottom',
            'footer_firstcolumn' => 'Footer first column',
            'footer_secondcolumn' => 'Footer second column',
            'footer_thirdcolumn' => 'Footer third column',
            'footer_fourthcolumn' => 'Footer fourth column',
            'footer-a' => 'Footer a',
            'footer-b' => 'Footer b',
          ),
          'settings' => 
          array (
            'theme_color_palette' => 'turquoise',
            'theme_bg_pattern' => 'none',
            'boson_boxed' => '0',
          ),
          'version' => NULL,
        ),
        'project' => '',
        'version' => NULL,
      ),
    ),
  ),
  'profiles' => 
  array (
    'minimal' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
    'standard' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
  ),
);