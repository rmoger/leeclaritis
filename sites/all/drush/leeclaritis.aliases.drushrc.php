<?php


  $aliases["vagrant"] = array (
    'uri' => 'http://www.maxt.local',
    'root' => '/var/www/site/docroot',

    'remote-host' => '172.16.93.130',
    'remote-user' => 'vagrant',
    'ssh-options' => '-p 22 -i /Users/rich/.vagrant.d/insecure_private_key',
    '#check-local' => TRUE,
    array (
      '%files' => 'sites/default/files',
    ),
  );

  $aliases["stage3"] = array (
    'root' => '/var/www/leeclaritis',
    'uri' => 'http://leeclaritis.p3.claritis.co.uk',
    'remote-host' => '54.77.238.59',
    'remote-user' => 'rich',
    array (
      '%files' => 'sites/default/files',
    ),
  );



