<?php

/**
 * Base class for field analysis reports.
 */
class FieldAnalyticsReports {

  /**
   * The field instance.
   *
   * @var array
   */
  protected $instance;

  /**
   * The loaded field.
   *
   * @var array
   */
  protected $field;

  /**
   * Shared string replacements suitable for t().
   *
   * @var array
   */
  protected $replacements;

  /**
   * Define defaults for a report.
   */
  public function __construct($instance) {
    $this->instance = $instance;
    $this->field = field_info_field($instance['field_name']);

    // Shared string replacements.
    $bundles = field_info_bundles();
    $this->replacements = array(
      '%field' => $this->instance['label'],
      '%bundle' => $bundles[$this->instance['entity_type']][$this->instance['bundle']]['label'],
    );
  }

  /**
   * Generate the data for a report.
   */
  public function data() {}

  /**
   * Renders a report output.
   */
  public function render() {}

  /**
   * Gets a count of entities for this bundle.
   */
  public function countEntities() {
    $query = new FieldAnalyticsQuery($this->instance);
    $query->count();
    return $query->execute();
  }

  /**
   * Gets a count of entities with this field value set.
   */
  public function countHasValue() {
    $query = new FieldAnalyticsQuery($this->instance);
    $query->isNotNull($this->instance['field_name'], field_analytics_get_value_column($this->field))
      ->count();
    return $query->execute();
  }

  /**
   * Gets a list of entities with this field value set.
   */
  public function entityHasValue() {
    $query = new FieldAnalyticsQuery($this->instance);
    $query->isNotNull($this->instance['field_name']);
    $result = $query->execute();
    $entity_type = $this->instance['entity_type'];
    return entity_load($entity_type, array_keys($result[$entity_type]));
  }

  /**
   * Gets a list of field values.
   */
  public function entityFieldValues() {
    $values = array();
    //$entities = entity_load_multiple_by_name($this->instance['entity_type'], array(201, 202, 203));
    $entities = $this->entityHasValue();

    foreach ($entities as $entity) {
      if ($items = field_get_items($this->instance['entity_type'], $entity, $this->instance['field_name'])) {
        // Take multiple field values into account.
        foreach ($items as $item) {
          // Get raw value.
          $raw = $item['value'];
          if (isset($raw) && is_numeric($raw)) {
            $values[] = $raw;
          }
          else {
            // Sanitize values.
            $view_value = field_view_value($this->instance['entity_type'], $entity, $this->instance['field_name'], $item);
            // Filter out empty values for more accuracy. The information about
            // is available whether values have been set is available in
            // FieldAnalyticsReportValueEntered.
            if (!empty($view_value['#markup'])) {
              $values[] = $view_value['#markup'];
            }
          }
        }
      }
    }
    return $values;
  }

  /**
   * Calculates a percent.
   */
  public function getPercent($number, $total) {
    // Try not to rip a hole in the fabric of the universe.
    // @link http://en.wikipedia.org/wiki/Division_by_zero @endlink
    return $total > 0 ? ($number / $total) * 100 : 0;
  }

  /**
   * Calculates an average.
   *
   * @param array $values
   *   An array of numeric values to find an average.
   *
   * @return int
   *   An average number.
   */
  public function getAverage($values = array()) {
    $average = 0;

    if (count($values) > 1 ) {
      $average = (array_sum($values) / count($values));
    }
    elseif (count($values) == 1) {
      $average = current($values);
    }

    return $average;
  }

  /**
   * Filter form for Field analytics Entity Field Query.
   *
   * @see FieldAnalyticsQuery::__construct()
   */
  public function filterForm($form, &$form_state) {
    $form['filters'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'field-analytics-filters',
      ),
      '#tree' => TRUE,
    );

    // Allow per field reports to be filtered by the values of any other field
    // in the bundle.
    $filter_fields = variable_get("field_analytics_filter_fields_{$this->instance['entity_type']}_{$this->instance['bundle']}", array());

    // Only include non-empty array values. We do this because checkboxes - such
    // as those used on the per-bundle settings form - store empty values.
    foreach (array_filter($filter_fields) as $field_name) {
      // Get allowed values.
      $instance = field_info_instance($this->instance['entity_type'], $field_name, $this->instance['bundle']);
      $allowed_values = field_analytics_get_allowed_values($instance);

      $form['filters'][$field_name] = array(
        '#type' => 'select',
        '#title' => $instance['label'],
        '#options' => $allowed_values,
        '#default_value' => isset($_GET['filters'][$field_name]) ? $_GET['filters'][$field_name] : NULL,
        '#empty_value' => '--',
      );
    }

    // Check if there are any configured fields to filter on.
    if (!empty($filter_fields)) {
      // Set a filter form container faux title.
      $form['filters']['#prefix'] = '<h2>' . t('Filter by field values') . '</h2>';

      // Use the automatic submit handler added by drupal_prepare_form().
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Filter'),
      );

      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => t('Reset'),
        '#submit' => array('field_analytics_filter_form_reset'),
      );
    }

    return $form;
  }

  /**
   * Submit callback for filterForm().
   */
  public function filterFormSubmit($form, &$form_state) {
    $values = $form_state['values'];

    $filter_query = array();
    if (is_array($values['filters'])) {
      foreach ($values['filters'] as $key => $value) {
        // Allow filters to be optional.
        if ($value !== $form['filters'][$key]['#empty_value']) {
          $filter_query[$key] = $value;
        }
      }
      $options = !empty($filter_query) ? array('query' => array('filters' => $filter_query)) : array();
      $form_state['redirect'] = array(current_path(), $options);
    }
  }

  /**
   * Submit callback for filterForm().
   */
  public function filterFormReset($form, &$form_state) {
    $form_state['redirect'] = array(current_path());
  }

  /**
   * Defines a list of supported report types by field type.
   *
   * @return array
   *   An array of supported report type class names.
   */
  protected function reportTypes() {
    $report_types = array();

    switch ($this->field['type']) {
      // Core field types.
      case 'file':
      case 'image':
        $report_types[] = 'FieldAnalyticsReportValueEntered';
        break;
      case 'list_integer':
      case 'list_float':
      case 'list_text':
      case 'list_boolean':
      case 'taxonomy_term_reference':
        $report_types[] = 'FieldAnalyticsReportValueEntered';
        $report_types[] = 'FieldAnalyticsReportAllowedValues';
        break;
      case 'number_integer':
      case 'number_decimal':
      case 'number_float':
        $report_types[] = 'FieldAnalyticsReportValueEntered';
        $report_types[] = 'FieldAnalyticsReportAverageValue';
        break;
      case 'text':
      case 'text_long':
      case 'text_with_summary':
        $report_types[] = 'FieldAnalyticsReportValueEntered';
        $report_types[] = 'FieldAnalyticsReportAverageLength';
        break;

      // Contrib field types.
      case 'entityreference':
        $report_types[] = 'FieldAnalyticsReportValueEntered';
        break;
    }

    // Allow other modules to alter the supported field analytics report types.
    drupal_alter('field_analytics_report_types', $report_types, $this->instance);

    return $report_types;
  }

  /**
   * Generates reports for a field instance.
   */
  public function reports() {
    $reports = array();

    // Get reports for this field.
    foreach ($this->reportTypes() as $class_name) {
      if (class_exists($class_name)) {
        $report = new $class_name($this->instance);
        $reports[] = '<div class="field-analytics-report">' . $report->render() . '</div>';
      }
    }

    return implode("\n", $reports);
  }

  /**
   * Renders a basic report line.
   */
  public function reportLines($lines = array()) {
    $output = '';
    foreach ($lines as $line) {
      $output .= '<div class="field field-label-inline">';
      $output .= '<div class="field-label inline">' . $line['label'] . ':&nbsp;</div>';
      $output .= $line['value'];
      $output .= '</div>';
    }

    return $output;
  }

}

/**
 * Generates a report on the number of entities with this field value set,
 * the total number of entities, and percentage.
 */
class FieldAnalyticsReportValueEntered extends FieldAnalyticsReports {

  public function render() {
    $replacements = $this->replacements;

    // Build the data table.
    $headers = array('', t('Number of %bundle entities', $replacements), t('Percentage of total %bundle entities', $replacements));
    $rows = array();

    // Value entered.
    $percent = $this->getPercent($this->countHasValue(), $this->countEntities());
    $rows[] = array(t('Value entered'), $this->countHasValue(), number_format($percent, 2) . '%');

    // No value entered.
    $noValue = $this->countEntities() - $this->countHasValue();
    if ($noValue > 0) {
      $percent = $this->getPercent($noValue, $this->countEntities());
      $rows[] = array(t('No value entered'), $noValue, number_format($percent, 2) . '%');
    }

    // Build the output.
    $output = '';
    $output .= '<h2>' . t('Value entered') . '</h2>';

    $table = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No data available.'));

    $output .= drupal_render($table);

    return $output;
  }

}

/**
 * Generates a report on the allowed values of a field.
 */
class FieldAnalyticsReportAllowedValues extends FieldAnalyticsReportValueEntered {

  public function data() {
    // Get allowed values.
    $allowed_values = field_analytics_get_allowed_values($this->instance);

    // Build a data array.
    $data = array();
    foreach ($allowed_values as $key => $label) {
      // Get number of entities with this field value set.
      $query = new FieldAnalyticsQuery($this->instance);
      $query->fieldCondition($this->instance['field_name'], field_analytics_get_value_column($this->field), $key, '=')
        ->count();
      $data[$key] = array(
        'label' => $label,
        'count' => $query->execute(),
      );
    }

    return $data;
  }

  public function render() {
    $data = $this->data();
    $replacements = $this->replacements;

    // Build the data table.
    $headers = array(t('Value'), t('Number of %bundle entities with this value set', $replacements), t('Percentage of total %bundle entities with this value set', $replacements));
    $rows = array();
    foreach ($data as $info) {
      $percent = $this->getPercent($info['count'], $this->countHasValue());
      $rows[] = array($info['label'], $info['count'], number_format($percent, 2) . '%');
    }

    // Build the output.
    $output = '';
    $output .= '<h2>' . t('Allowed values') . '</h2>';

    $table = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No data available.'));

    $output .= drupal_render($table);

    return $output;
  }

}

/**
 * Generates a report of the average value.
 */
class FieldAnalyticsReportAverageValue extends FieldAnalyticsReportValueEntered {

  public function data() {
    // Get the average.
    $values = $this->entityFieldValues();
    return $this->getAverage($values);
  }

  public function render() {
    $lines = array();
    $lines[] = array(
      'label' => t('Average value of %field', $this->replacements),
      'value' => $this->data(),
    );

    return $this->reportLines($lines);
  }

}

/**
 * Generates a report of the average field value length.
 */
class FieldAnalyticsReportAverageLength extends FieldAnalyticsReportValueEntered {

  public function render() {
    $values = $this->entityFieldValues();
    $lengths = array();
    foreach ($values as $value) {
      $lengths[] = mb_strlen($value);
    }
    $data = $this->getAverage($lengths);

    $lines = array();
    $lines[] = array(
      'label' => t('Average length of %field', $this->replacements),
      'value' => t('@count characters', array('@count' => number_format($data))),
    );

    $output = '';
    $output .= '<h2>' . t('Average length') . '</h2>';

    $output .= $this->reportLines($lines);

    return $output;
  }

}
