<?php
/**
 * @file
 * treaties2.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function treaties2_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function treaties2_node_info() {
  $items = array(
    'treaty' => array(
      'name' => t('treaty'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
