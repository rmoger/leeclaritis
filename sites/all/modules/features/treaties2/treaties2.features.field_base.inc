<?php
/**
 * @file
 * treaties2.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function treaties2_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_adoption_year'
  $field_bases['field_adoption_year'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_adoption_year',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 0,
        'hour' => 0,
        'minute' => 0,
        'month' => 0,
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => '',
      'todate' => '',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'datetime',
  );

  // Exported field_base: 'field_full_title'
  $field_bases['field_full_title'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_full_title',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_instrument_type'
  $field_bases['field_instrument_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_instrument_type',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Treaty' => 'Treaty',
        'EU Directive' => 'EU Directive',
        'EU Regulation' => 'EU Regulation',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_treaty_region'
  $field_bases['field_treaty_region'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_treaty_region',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'International' => 'International',
        'Council of Europe' => 'Council of Europe',
        'African Union (AU)' => 'African Union (AU)',
        'European Union' => 'European Union (EU)',
        'Organization of American States (OAS)' => 'Organization of American States (OAS)',
        'League of Arab States' => 'League of Arab States',
        'South Asian Association for Regional Cooperation (SAARC)' => 'South Asian Association for Regional Cooperation (SAARC)',
        'Organization of Islamic Cooperation (OIC)' => 'Organization of Islamic Cooperation (OIC)',
        'Association of Southeast Asian Nations (ASEAN)' => 'Association of Southeast Asian Nations (ASEAN)',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
