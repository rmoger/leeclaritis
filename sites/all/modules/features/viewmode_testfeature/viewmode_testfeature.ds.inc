<?php
/**
 * @file
 * viewmode_testfeature.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function viewmode_testfeature_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|detention_centre|featuretest';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'detention_centre';
  $ds_fieldsetting->view_mode = 'featuretest';
  $ds_fieldsetting->settings = array(
    'summary_link' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '49',
      'label' => 'hidden',
      'format' => 'author',
    ),
  );
  $export['node|detention_centre|featuretest'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function viewmode_testfeature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|detention_centre|featuretest';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'detention_centre';
  $ds_layout->view_mode = 'featuretest';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'summary_link',
        1 => 'title',
        2 => 'field_country_dc_ref',
        3 => 'field_i_b_3',
        4 => 'field_i_b_2_1',
        5 => 'field_i_b_3_1',
        6 => 'field_i_b_2_2',
        7 => 'field_i_b_3_3',
        8 => 'field_i_b_5',
        9 => 'field_address_contact_informatio',
        10 => 'field_i_c_9_1',
        11 => 'field_i_c_9_2',
        12 => 'field_i_c_10_1',
        13 => 'field_i_c_10_2',
        14 => 'field_i_c_11_1',
        15 => 'field_i_c_10_3',
        16 => 'field_i_c_11_2',
        17 => 'field_i_d_2',
        18 => 'field_i_d_4',
        19 => 'field_i_c_1_1',
        20 => 'field_i_c_2_1',
        21 => 'field_i_c_1_2',
        22 => 'field_i_c_3_1',
        23 => 'field_i_c_2_2',
        24 => 'field_i_c_3_4',
        25 => 'field_i_c_5',
        26 => 'field_i_d_5',
        27 => 'field_i_c_3_3',
        28 => 'field_i_c_6_1',
        29 => 'field_i_c_7_1',
        30 => 'field_i_c_7_2',
        31 => 'field_i_c_6_3',
        32 => 'field_i_c_4_1',
        33 => 'field_i_c_7_3',
        34 => 'field_i_c_6_2',
        35 => 'field_i_c_7_6',
        36 => 'field_i_c_6_4',
        37 => 'field_i_c_7_5',
        38 => 'field_i_c_7_7',
        39 => 'group_feature_test_group',
        40 => 'field_i_c_7_8',
        41 => 'field_i_c_8_1',
        42 => 'field_i_c_2_a',
        43 => 'field_i_d_3_1',
        44 => 'field_i_d_3_2',
        45 => 'field_geo_x',
        46 => 'field_geo_y',
        47 => 'field_zoom',
        48 => 'field_map',
      ),
      'ds_hidden' => array(
        49 => 'author',
        50 => 'body',
        51 => 'field_i_c_2',
      ),
    ),
    'fields' => array(
      'summary_link' => 'ds_content',
      'title' => 'ds_content',
      'field_country_dc_ref' => 'ds_content',
      'field_i_b_3' => 'ds_content',
      'field_i_b_2_1' => 'ds_content',
      'field_i_b_3_1' => 'ds_content',
      'field_i_b_2_2' => 'ds_content',
      'field_i_b_3_3' => 'ds_content',
      'field_i_b_5' => 'ds_content',
      'field_address_contact_informatio' => 'ds_content',
      'field_i_c_9_1' => 'ds_content',
      'field_i_c_9_2' => 'ds_content',
      'field_i_c_10_1' => 'ds_content',
      'field_i_c_10_2' => 'ds_content',
      'field_i_c_11_1' => 'ds_content',
      'field_i_c_10_3' => 'ds_content',
      'field_i_c_11_2' => 'ds_content',
      'field_i_d_2' => 'ds_content',
      'field_i_d_4' => 'ds_content',
      'field_i_c_1_1' => 'ds_content',
      'field_i_c_2_1' => 'ds_content',
      'field_i_c_1_2' => 'ds_content',
      'field_i_c_3_1' => 'ds_content',
      'field_i_c_2_2' => 'ds_content',
      'field_i_c_3_4' => 'ds_content',
      'field_i_c_5' => 'ds_content',
      'field_i_d_5' => 'ds_content',
      'field_i_c_3_3' => 'ds_content',
      'field_i_c_6_1' => 'ds_content',
      'field_i_c_7_1' => 'ds_content',
      'field_i_c_7_2' => 'ds_content',
      'field_i_c_6_3' => 'ds_content',
      'field_i_c_4_1' => 'ds_content',
      'field_i_c_7_3' => 'ds_content',
      'field_i_c_6_2' => 'ds_content',
      'field_i_c_7_6' => 'ds_content',
      'field_i_c_6_4' => 'ds_content',
      'field_i_c_7_5' => 'ds_content',
      'field_i_c_7_7' => 'ds_content',
      'group_feature_test_group' => 'ds_content',
      'field_i_c_7_8' => 'ds_content',
      'field_i_c_8_1' => 'ds_content',
      'field_i_c_2_a' => 'ds_content',
      'field_i_d_3_1' => 'ds_content',
      'field_i_d_3_2' => 'ds_content',
      'field_geo_x' => 'ds_content',
      'field_geo_y' => 'ds_content',
      'field_zoom' => 'ds_content',
      'field_map' => 'ds_content',
      'author' => 'ds_hidden',
      'body' => 'ds_hidden',
      'field_i_c_2' => 'ds_hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|detention_centre|featuretest'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function viewmode_testfeature_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'featuretest';
  $ds_view_mode->label = 'featuretest';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['featuretest'] = $ds_view_mode;

  return $export;
}
