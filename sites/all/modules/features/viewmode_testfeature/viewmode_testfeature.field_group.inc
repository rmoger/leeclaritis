<?php
/**
 * @file
 * viewmode_testfeature.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function viewmode_testfeature_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feature_test_group|node|detention_centre|featuretest';
  $field_group->group_name = 'group_feature_test_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'detention_centre';
  $field_group->mode = 'featuretest';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Test group',
    'weight' => '39',
    'children' => array(
      0 => 'field_i_c_8_1',
      1 => 'field_i_c_2_a',
      2 => 'field_i_c_7_8',
      3 => 'field_i_d_3_1',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_feature_test_group|node|detention_centre|featuretest'] = $field_group;

  return $export;
}
