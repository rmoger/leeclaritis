<?php
/**
 * @file
 * domain_context.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function domain_context_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'contextual_view_modes_global';
  $strongarm->value = array(
    'node' => array(
      0 => array(
        'bundle' => 'country',
        'context' => 'public_website',
        'view_mode' => 'website_full',
      ),
    ),
  );
  $export['contextual_view_modes_global'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'context_block_rebuild_needed';
  $strongarm->value = TRUE;
  $export['context_block_rebuild_needed'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cvm_enabled_content_types';
  $strongarm->value = array(
    0 => 'article',
    1 => 'page',
    2 => 'publication',
    3 => 'source',
    4 => 'webform',
    5 => 'country',
    6 => 'detention_centre',
    7 => 'test_entity',
    8 => 'treaty',
    9 => 'treaty_reservations',
  );
  $export['cvm_enabled_content_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cvm_global_content_type_modes';
  $strongarm->value = array(
    'page' => array(
      'public_website' => 'website_full',
    ),
    'country' => array(
      'public_website' => 'website_full',
    ),
    'detention' => array(
      'centre_public_website' => 'website_full',
    ),
  );
  $export['cvm_global_content_type_modes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'domain_bootstrap_modules';
  $strongarm->value = array();
  $export['domain_bootstrap_modules'] = $strongarm;

  return $export;
}
