<?php
/**
 * @file
 * domain_context.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function domain_context_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access contextual links'.
  $permissions['access contextual links'] = array(
    'name' => 'access contextual links',
    'roles' => array(
      'administrator' => 'administrator',
      'gdpadmin' => 'gdpadmin',
      'snisadmin' => 'snisadmin',
    ),
    'module' => 'contextual',
  );

  // Exported permission: 'administer contexts'.
  $permissions['administer contexts'] = array(
    'name' => 'administer contexts',
    'roles' => array(),
    'module' => 'context_ui',
  );

  // Exported permission: 'administer cvm settings'.
  $permissions['administer cvm settings'] = array(
    'name' => 'administer cvm settings',
    'roles' => array(),
    'module' => 'contextual_view_modes',
  );

  // Exported permission: 'set view modes per node'.
  $permissions['set view modes per node'] = array(
    'name' => 'set view modes per node',
    'roles' => array(),
    'module' => 'contextual_view_modes',
  );

  return $permissions;
}
