<?php
/**
 * @file
 * domain_context.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function domain_context_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'public_website';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        2 => 2,
        3 => 3,
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;
  $export['public_website'] = $context;

  return $export;
}
