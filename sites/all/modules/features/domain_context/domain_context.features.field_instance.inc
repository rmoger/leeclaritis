<?php
/**
 * @file
 * domain_context.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function domain_context_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-article-field_cvm_cvm'
  $field_instances['node-article-field_cvm_cvm'] = array(
    'bundle' => 'article',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 11,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-country-field_cvm_cvm'
  $field_instances['node-country-field_cvm_cvm'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'country_scorecard' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 197,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'website_full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 157,
    ),
  );

  // Exported field_instance: 'node-detention_centre-field_cvm_cvm'
  $field_instances['node-detention_centre-field_cvm_cvm'] = array(
    'bundle' => 'detention_centre',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 64,
      ),
      'summary' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'website_full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 53,
    ),
  );

  // Exported field_instance: 'node-page-field_cvm_cvm'
  $field_instances['node-page-field_cvm_cvm'] = array(
    'bundle' => 'page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'node-publication-field_cvm_cvm'
  $field_instances['node-publication-field_cvm_cvm'] = array(
    'bundle' => 'publication',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 42,
    ),
  );

  // Exported field_instance: 'node-source-field_cvm_cvm'
  $field_instances['node-source-field_cvm_cvm'] = array(
    'bundle' => 'source',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 38,
    ),
  );

  // Exported field_instance: 'node-test_entity-field_cvm_cvm'
  $field_instances['node-test_entity-field_cvm_cvm'] = array(
    'bundle' => 'test_entity',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'node-treaty-field_cvm_cvm'
  $field_instances['node-treaty-field_cvm_cvm'] = array(
    'bundle' => 'treaty',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-treaty_reservations-field_cvm_cvm'
  $field_instances['node-treaty_reservations-field_cvm_cvm'] = array(
    'bundle' => 'treaty_reservations',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-webform-field_cvm_cvm'
  $field_instances['node-webform-field_cvm_cvm'] = array(
    'bundle' => 'webform',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cvm_cvm',
    'label' => 'Contextual View Mode',
    'required' => FALSE,
    'settings' => array(
      'cardinality' => -1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => NULL,
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => NULL,
      'weight' => 31,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contextual View Mode');

  return $field_instances;
}
