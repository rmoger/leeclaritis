<?php
/**
 * @file
 * domain_context.domains.inc
 */

/**
 * Implements hook_domain_default_domains().
 */
function domain_context_domain_default_domains() {
  $domains = array();
  $domains['gdp_local'] = array(
    'subdomain' => 'gdp.local',
    'sitename' => 'GDP Database Local',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -1,
    'is_default' => 1,
    'machine_name' => 'gdp_local',
  );
  $domains['www_gdp_local'] = array(
    'subdomain' => 'wwww.globaldetentionproject.org',
    'sitename' => 'Public Website',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => 0,
    'is_default' => 0,
    'machine_name' => 'www_gdp_local',
  );

  return $domains;
}

/**
 * Implements hook_domain_theme_default_themes().
 */
function domain_context_domain_theme_default_themes() {
  $domain_themes = array();
  $domain_themes['gdp_local'] = array(
    'bartik' => array(
      'theme' => 'bartik',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
  );
  $domain_themes['www_gdp_local'] = array();

  return $domain_themes;
}
