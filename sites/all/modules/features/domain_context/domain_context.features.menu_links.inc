<?php
/**
 * @file
 * domain_context.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function domain_context_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/structure/context
  $menu_links['management:admin/structure/context'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/context',
    'router_path' => 'admin/structure/context',
    'link_title' => 'Context',
    'options' => array(
      'attributes' => array(
        'title' => 'Associate menus, views, blocks, etc. with different contexts to structure your site.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/structure',
  );
  // Exported menu link: management:admin/structure/cvm
  $menu_links['management:admin/structure/cvm'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/cvm',
    'router_path' => 'admin/structure/cvm',
    'link_title' => 'Contextual View Modes',
    'options' => array(
      'attributes' => array(
        'title' => 'Settings and configuration options for contextual view modes',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/structure',
  );
  // Exported menu link: management:admin/structure/domain
  $menu_links['management:admin/structure/domain'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/domain',
    'router_path' => 'admin/structure/domain',
    'link_title' => 'Domains',
    'options' => array(
      'attributes' => array(
        'title' => 'Manage and configure domains.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/structure',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Context');
  t('Contextual View Modes');
  t('Domains');


  return $menu_links;
}
