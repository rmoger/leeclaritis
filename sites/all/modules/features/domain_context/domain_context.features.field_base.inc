<?php
/**
 * @file
 * domain_context.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function domain_context_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_cvm_cvm'
  $field_bases['field_cvm_cvm'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_cvm_cvm',
    'foreign keys' => array(
      'context' => array(
        'columns' => array(
          'context' => 'name',
        ),
        'table' => 'context',
      ),
      'view_mode' => array(
        'columns' => array(
          'view_mode' => 'view_mode',
        ),
        'table' => 'ds_view_modes',
      ),
    ),
    'indexes' => array(
      'context' => array(
        0 => 'context',
      ),
      'view_context' => array(
        0 => 'view_mode',
        1 => 'context',
      ),
      'view_mode' => array(
        0 => 'view_mode',
      ),
    ),
    'locked' => 0,
    'module' => 'contextual_view_modes',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'contextual_view_modes_cvm',
  );

  return $field_bases;
}
